import yaml
from enum import Enum

import rpmaster_log
import rpmaster_save
import rpmaster_game_file_equation
import rpmaster_game_file_variable

class ConditionGroupTypes(Enum):
    INVALID = 'invalid'
    AND = 'and_gate'
    OR = 'or_gate'

class ConditionTypes(Enum):
    INVALID = 'invalid'
    VARIABLE_A_MATCH_VARIABLE_B = 'variable_a_match_variable_b'
    VARIABLE_A_MATCH_VALUE = 'variable_a_match_value'
    EQUATION_ABOVE_VALUE = 'equation_above_value'
    EQUATION_BELOW_VALUE = 'equation_below_value'
    

defaultConditionGroupComments = 'None'
defaultConditionGroupType = ConditionGroupTypes.INVALID

defaultConditionComments = 'None'
defaultConditionType = ConditionTypes.INVALID
defaultEquation = 'invalid'

# only standard YAML tag
class ConditionGroup(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileConditionGroup'
    Name = ''
    Comments = ''
    Type = None
    Conditions = []

    def __init__(self, Name, Type):
        self.Name = Name
        self.Comments = defaultConditionGroupComments
        self.Type = Type

        self.Conditions = []

    def __repr__(self):
        return "%s(Name=%r, Comments=%r, Type=%r, Conditions=%r, )" % (
            self.__class__.__name__, self.Name, self.Comments, self.Type, self.Conditions, )

    def is_valid_condition_group(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Condition group has no name")
            return result

        if self.Type == ConditionGroupTypes.INVALID:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Condition group has invalid type")
            return result

        for condition in self.Conditions:
            conRef = rpmaster_save.main_state().get_condition_ref(condition)
            if conRef == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find condition " + condition + " from: " + self.Name)
            if(conRef.is_valid_condition() == False):
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found invalid condition: " + self.Name + ' / ' + condition.Name)
                return
            pass
            
        return result


class Condition(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileCondition'
    Name = ''
    Comments = ''

    Type = None

    VariableA = ''
    VariableB = ''
    Value = None
    Equation = ''
    VariablesUsed = [] # array of name references

    def __init__(self, Name, Type):
        self.Name = Name
        self.Comments = defaultConditionComments
        self.Type = Type

        self.VariableA = ''
        self.VariableB = ''
        self.Value = None
        self.Equation = defaultEquation
        self.VariablesUsed = [] # array of name references

    def __repr__(self):
        return "%s(Name=%r, Comments=%r, Type=%r, VariableA=%r, VariableB=%r, Value=%r, Equation=%r, VariablesUsed=%r, )" % (
            self.__class__.__name__, self.Name, self.Comments, self.Type, self.VariableA, self.VariableB, self.Value, self.Equation, self.VariablesUsed, )

    def is_valid_condition(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Condition has no name")
            return result
            
        return result


def evaluate_condition_group(conditionGroup):
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating condition group: " + conditionGroup.Name)

    if(conditionGroup.is_valid_condition_group() == False):
        return -1

    result = -1

    if conditionGroup.Type == ConditionGroupTypes.AND: # Only return true if all conditions pass
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "AND condition group")
        result = True
        for condition in conditionGroup.Conditions:
            conRef = rpmaster_save.main_state().get_condition_ref(condition)
            if evaluate_condition(conRef) == False:
                result = False
                pass
            pass
        pass
    elif conditionGroup.Type == ConditionGroupTypes.OR: # Return true if any conditions pass
        result = False
        for condition in conditionGroup.Conditions:
            conRef = rpmaster_save.main_state().get_condition_ref(condition)
            if evaluate_condition(conRef) == True:
                result = True
                pass
            pass
        pass

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Final condition result: " + str(result))

    return result

def evaluate_condition(condition):
    if isinstance(condition, ConditionGroup): # Support both groups and single conditions
        result = evaluate_condition_group(condition)
        pass
    elif isinstance(condition, Condition):
        if(condition.is_valid_condition() == False):
            return

        result = False

        if condition.Type == ConditionTypes.VARIABLE_A_MATCH_VARIABLE_B:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating VARIABLE_A_MATCH_VARIABLE_B condition: " + condition.Name)

            varARef = rpmaster_save.main_state().get_variable_ref(condition.VariableA)
            varBRef = rpmaster_save.main_state().get_variable_ref(condition.VariableB)

            varAVal = varARef.get_variable_value()
            varBVal = varBRef.get_variable_value()

            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Variable A value: " + str(varAVal))
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Variable B value: " + str(varBVal))

            if(varAVal == varBVal):
                result = True
            pass
        elif condition.Type == ConditionTypes.VARIABLE_A_MATCH_VALUE:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating VARIABLE_A_MATCH_VALUE condition: " + condition.Name)

            varARef = rpmaster_save.main_state().get_variable_ref(condition.VariableA)

            varAVal = varARef.get_variable_value()

            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Variable A value: " + str(varAVal))
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Comparison value: " + str(condition.Value))

            if(varAVal == condition.Value):
                result = True
            pass
        elif condition.Type == ConditionTypes.EQUATION_ABOVE_VALUE:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating EQUATION_ABOVE_VALUE condition: " + condition.Name)
            value = rpmaster_game_file_equation.evaluate(condition.VariablesUsed, condition.Equation)
            if(value >= condition.Value):
                result = True
            pass
        elif condition.Type == ConditionTypes.EQUATION_BELOW_VALUE:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating EQUATION_BELOW_VALUE condition: " + condition.Name)
            value = rpmaster_game_file_equation.evaluate(condition.VariablesUsed, condition.Equation)
            if(value < condition.Value):
                result = True
            pass

        if result == True:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Condition result: True")
            pass
        else:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Condition result: False")
            pass
        pass

    return result
