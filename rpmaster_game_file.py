import yaml
import io
import os
# from yaml import SafeLoader, SafeDumper
from yaml import Loader, Dumper
from os import path

import copy
import datetime

import rpmaster_settings
import rpmaster_log
import rpmaster_time
import rpmaster_game_file_variable
import rpmaster_game_file_condition
import rpmaster_game_file_operation
import rpmaster_game_file_event
import rpmaster_game_file_quiz
import rpmaster_game_file_operation_action
import rpmaster_game_file_operation_threshold
import rpmaster_game_file_string
import rpmaster_game_file_collection
import rpmaster_game_file_root_menu_variable
import rpmaster_game_file_root_menu_status
import rpmaster_game_file_menu_variable_threshold
import rpmaster_game_file_menu_status_threshold

import custom_gamefile_generator


currentGameFileVersion = '0.0'
defaultGameFileName = 'New Game File'
defaultGameFileVersion = '1.0'
defaultGameFileAuthor = 'Unknown'
defaultGameFileComments = 'None'

defaultEnableTime = False
# defaultStartTime = datetime.datetime.now().replace(hour=12, minute=0, second=0, microsecond=0)
defaultStartTime = datetime.datetime(1, 1, 1)


loadedGameFile = None

# only standard YAML tag


class GameFile(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFile'
    GameFileFileVersion = ''
    Name = ''
    Version = ''
    Author = ''
    Comments = ''
    
    LoadedCorrectly = False
    InitialisationOperation = ''

    EnableTime = False
    StartTime = False

    InitialVariables = []
    RootMenuVariables = []
    RootMenuStatusMessages = []
    Operations = []
    Actions = []
    ActionEquations = []
    Strings = []

    Events = []

    QuizGroups = []
    QuizQuestions = []
    QuizAnswers = []

    Collections = []

    ConditionGroups = []
    Conditions = []

    def __init__(self, Name=None):
        self.GameFileFileVersion = currentGameFileVersion
        if(Name is None):
            self.Name = defaultGameFileName
        else:
            self.Name = Name

        self.Version = defaultGameFileVersion
        self.Author = defaultGameFileAuthor
        self.Comments = defaultGameFileComments

        self.LoadedCorrectly = False
        self.InitialisationOperation = ''

        self.EnableTime = defaultEnableTime
        self.StartTime = defaultStartTime

        self.InitialVariables = []
        self.RootMenuVariables = []
        self.RootMenuStatusMessages = []
        self.Operations = []
        self.Actions = []
        self.ActionEquations = []
        self.Strings = []

        self.Events = []

        self.QuizGroups = []
        self.QuizQuestions = []
        self.QuizAnswers = []

        self.Collections = []

        self.ConditionGroups = []
        self.Conditions = []


    def __repr__(self):
        return "%s(GameFileFileVersion=%r, Name=%r, Version=%r, Author=%r, Comments=%r, LoadedCorrectly=%r, InitialisationOperation=%r, EnableTime=%r, StartTime=%r, InitialVariables=%r, RootMenuVariables=%r, RootMenuStatusMessages=%r, Operations=%r, Actions=%r, ActionEquations=%r, Strings=%r, Events=%r, QuizGroups=%r, QuizQuestions=%r, QuizAnswers=%r, Collections=%r, ConditionGroups=%r, Conditions=%r, )" % (
            self.__class__.__name__, self.GameFileFileVersion, self.Name, self.Version, self.Author, self.Comments, self.LoadedCorrectly, self.InitialisationOperation, self.EnableTime, self.StartTime, self.InitialVariables, self.RootMenuVariables, self.RootMenuStatusMessages, self.Operations, self.Actions, self.ActionEquations, self.Strings, self.Events, self.QuizGroups, self.QuizQuestions, self.QuizAnswers, self.Collections, self.ConditionGroups, self.Conditions, )

    def write_game_file(self, filePath):
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Writing game file: " + self.Name)
        self.LoadedCorrectly = False
        with open(filePath, 'w+') as gameFileFile:
            # yaml.safe_dump(gameFile, gameFileFile)
            # yaml.dump(gameFile, gameFileFile, canonical=True)

            yaml.Dumper.ignore_aliases = lambda *args : True
            yaml.dump(self, gameFileFile)
        return

def initialise():
    global loadedGameFile

    loadedGameFile = GameFile()
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Empty game file initialised")
    return

def load_game_file(gameFileName):
    global loadedGameFile

    gameFileDirectory = rpmaster_settings.loaded_settings().get_game_file_directory()
    filePath = gameFileDirectory + gameFileName + ".yaml"

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Loading game file: " + gameFileName)

    if(path.exists(filePath) == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "No save file found")
        return

    else:
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Game file found")
        with open(filePath, 'r') as filePath:
            # mainRPMasterState = yaml.safe_load(saveFile)
            loadedGameFile = yaml.load(filePath, Loader=Loader)
    loadedGameFile.LoadedCorrectly = True
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Game file loaded")

    if(loadedGameFile.GameFileFileVersion != currentGameFileVersion):
        rpmaster_log.debug_log(rpmaster_log.Level.WARNING, "Loaded game file was written by an old version and may cause errors")

    return


def is_game_file_path_valid(gameFileName):
    gameFileDirectory = rpmaster_settings.loaded_settings().get_game_file_directory()
    filePath = gameFileDirectory + gameFileName + ".yaml"
    if(path.exists(filePath) == False):
        return False
    else:
        return True


def is_game_file_loaded():
    return loadedGameFile.LoadedCorrectly


def get_loaded_game_file():
    if(is_game_file_loaded() == False):
        return None
    else:
        return loadedGameFile


def generate_game_file():
    gameFileDirectory = rpmaster_settings.loaded_settings().get_game_file_directory()
    filePath = gameFileDirectory + defaultGameFileName + ".yaml"

    gameFile = GameFile()

    if rpmaster_settings.loaded_settings().GenerateCustomGameFile == True:
        generate_custom_gamefile(gameFile)
        pass
    else:
        generate_example_gamefile(gameFile)
        pass


    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Checking to make sure we can write new game file: " + filePath)

    if(path.exists(filePath) == False):
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "No existing file")
        gameFile.write_game_file(filePath)
    else:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found existing new game file, delete it or rename it and try again: " + filePath)
    return

def generate_example_gamefile(gameFile):
    
    setGamefileSettings(gameFile)

    generateExampleVariables(gameFile)

    generateExampleMenuStatuses(gameFile)

    generateExampleConditions(gameFile)

    generateExampleOperationA(gameFile)
    generateExampleOperationB(gameFile)
    generateExampleOperationC(gameFile)
    generateExampleOperationD(gameFile)

    generateExampleEvents(gameFile)
    generateExampleQuizes(gameFile)

    return

def generate_custom_gamefile(gameFile):
    custom_gamefile_generator.generate_gamefile(gameFile)

    return

def setGamefileSettings(gameFile):
    gameFile.Comments = 'This is an example gamefile generated by RPM. It demonstrates most features of RPM, ranging from basic to advanced. You can copy elements from it to build your own gamefiles'
    
    gameFile.EnableTime = True
    gameFile.StartTime = datetime.datetime(1, 1, 1)

    gameFile.InitialisationOperation = 'EventTestOperationB'

    return

def generateExampleVariables(gameFile):
    intVariable = rpmaster_game_file_variable.Variable("ExampleInt")
    intVariable.PrettyName = "Example Int"
    intVariable.Type = rpmaster_game_file_variable.VariableTypes.INT
    intVariable.Value = 42
    gameFile.InitialVariables.append(intVariable)

    floatVariable = rpmaster_game_file_variable.Variable("ExampleFloat")
    floatVariable.PrettyName = "Example Float"
    floatVariable.Type = rpmaster_game_file_variable.VariableTypes.FLOAT
    floatVariable.Value = 420.69
    gameFile.InitialVariables.append(floatVariable)

    boolVariable = rpmaster_game_file_variable.Variable("ExampleBool")
    boolVariable.PrettyName = "Example Bool"
    boolVariable.Type = rpmaster_game_file_variable.VariableTypes.BOOL
    boolVariable.Value = True
    gameFile.InitialVariables.append(boolVariable)

    hiddenVariable = rpmaster_game_file_variable.Variable("ExampleHidden")
    hiddenVariable.PrettyName = "Example hidden variable"
    hiddenVariable.Type = rpmaster_game_file_variable.VariableTypes.INT
    hiddenVariable.Value = 80085
    hiddenVariable.AlwaysHideFromPlayer = True
    gameFile.InitialVariables.append(hiddenVariable)


    OpExampleVariable1 = rpmaster_game_file_variable.Variable("OpExampleVariable1")
    OpExampleVariable1.PrettyName = "Example op variable 1"
    OpExampleVariable1.Comments = "Int variable for use by example operations"
    OpExampleVariable1.Type = rpmaster_game_file_variable.VariableTypes.INT
    OpExampleVariable1.Value = 0

    OpExampleVariable2 = rpmaster_game_file_variable.Variable("OpExampleVariable2")
    OpExampleVariable2.PrettyName = "Example op variable 2"
    OpExampleVariable2.Comments = "Int variable for use by example operations"
    OpExampleVariable2.Type = rpmaster_game_file_variable.VariableTypes.INT
    OpExampleVariable2.Value = 1

    OpExampleVariable3 = rpmaster_game_file_variable.Variable("OpExampleVariable3")
    OpExampleVariable3.PrettyName = "Example op variable 3"
    OpExampleVariable3.Comments = "Float variable for use by example operations"
    OpExampleVariable3.Type = rpmaster_game_file_variable.VariableTypes.FLOAT
    OpExampleVariable3.Value = 2.0

    gameFile.InitialVariables.append(OpExampleVariable1)
    gameFile.InitialVariables.append(OpExampleVariable2)
    gameFile.InitialVariables.append(OpExampleVariable3)


    CoExampleVariable1 = rpmaster_game_file_variable.Variable("CoExampleVariable1")
    CoExampleVariable1.PrettyName = "Example condition variable 1"
    CoExampleVariable1.Comments = "Int variable for use by example conditions"
    CoExampleVariable1.Type = rpmaster_game_file_variable.VariableTypes.INT
    CoExampleVariable1.Value = 34

    CoExampleVariable2 = rpmaster_game_file_variable.Variable("CoExampleVariable2")
    CoExampleVariable2.PrettyName = "Example condition variable 2"
    CoExampleVariable2.Comments = "Int variable for use by example conditions"
    CoExampleVariable2.Type = rpmaster_game_file_variable.VariableTypes.INT
    CoExampleVariable2.Value = 34

    CoExampleVariable3 = rpmaster_game_file_variable.Variable("CoExampleVariable3")
    CoExampleVariable3.PrettyName = "Example condition variable 3"
    CoExampleVariable3.Comments = "Int variable for use by example conditions"
    CoExampleVariable3.Type = rpmaster_game_file_variable.VariableTypes.INT
    CoExampleVariable3.Value = 35


    gameFile.InitialVariables.append(CoExampleVariable1)
    gameFile.InitialVariables.append(CoExampleVariable2)
    gameFile.InitialVariables.append(CoExampleVariable3)


    VisTestConditionA = rpmaster_game_file_variable.Variable("VisTestConditionA")
    VisTestConditionA.PrettyName = "Visibility test variable 1 - conditionA"
    VisTestConditionA.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionA.Value = 0
    VisTestConditionA.VisibilityConditions.append('conditionA')
    VisTestConditionA.VisibilityConditions.append('conditionA')

    VisTestConditionB = rpmaster_game_file_variable.Variable("VisTestConditionB")
    VisTestConditionB.PrettyName = "Visibility test variable 2 - conditionB"
    VisTestConditionB.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionB.Value = 0
    VisTestConditionB.VisibilityConditions.append('conditionB')
    VisTestConditionB.VisibilityConditions.append('conditionB')

    VisTestConditionC = rpmaster_game_file_variable.Variable("VisTestConditionC")
    VisTestConditionC.PrettyName = "Visibility test variable 3 - conditionC"
    VisTestConditionC.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionC.Value = 0
    VisTestConditionC.VisibilityConditions.append('conditionC')

    VisTestConditionD = rpmaster_game_file_variable.Variable("VisTestConditionD")
    VisTestConditionD.PrettyName = "Visibility test variable 4 - conditionD"
    VisTestConditionD.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionD.Value = 0
    VisTestConditionD.VisibilityConditions.append('conditionD')

    VisTestConditionE = rpmaster_game_file_variable.Variable("VisTestConditionE")
    VisTestConditionE.PrettyName = "Visibility test variable 5 - conditionE"
    VisTestConditionE.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionE.Value = 0
    VisTestConditionE.VisibilityConditions.append('conditionE')

    VisTestConditionF = rpmaster_game_file_variable.Variable("VisTestConditionF")
    VisTestConditionF.PrettyName = "Visibility test variable 6 - conditionF"
    VisTestConditionF.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionF.Value = 0
    VisTestConditionF.VisibilityConditions.append('conditionF')

    VisTestConditionGroupA = rpmaster_game_file_variable.Variable("VisTestConditionGroupA")
    VisTestConditionGroupA.PrettyName = "Visibility test variable 7 - conditionGroupA"
    VisTestConditionGroupA.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionGroupA.Value = 0
    VisTestConditionGroupA.VisibilityConditions.append('ConditionGroupA')

    VisTestConditionGroupB = rpmaster_game_file_variable.Variable("VisTestConditionGroupB")
    VisTestConditionGroupB.PrettyName = "Visibility test variable 8 - conditionGroupB"
    VisTestConditionGroupB.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionGroupB.Value = 0
    VisTestConditionGroupB.VisibilityConditions.append('ConditionGroupB')

    VisTestConditionGroupC = rpmaster_game_file_variable.Variable("VisTestConditionGroupC")
    VisTestConditionGroupC.PrettyName = "Visibility test variable 9 - conditionGroupC"
    VisTestConditionGroupC.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionGroupC.Value = 0
    VisTestConditionGroupC.VisibilityConditions.append('ConditionGroupC')

    VisTestConditionGroupD = rpmaster_game_file_variable.Variable("VisTestConditionGroupD")
    VisTestConditionGroupD.PrettyName = "Visibility test variable 10 - conditionGroupD"
    VisTestConditionGroupD.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionGroupD.Value = 0
    VisTestConditionGroupD.VisibilityConditions.append('ConditionGroupD')

    VisTestConditionGroupE = rpmaster_game_file_variable.Variable("VisTestConditionGroupE")
    VisTestConditionGroupE.PrettyName = "Visibility test variable 11 - conditionGroupE"
    VisTestConditionGroupE.Type = rpmaster_game_file_variable.VariableTypes.INT
    VisTestConditionGroupE.Value = 0
    VisTestConditionGroupE.VisibilityConditions.append('ConditionGroupE')


    gameFile.InitialVariables.append(VisTestConditionA)
    gameFile.InitialVariables.append(VisTestConditionB)
    gameFile.InitialVariables.append(VisTestConditionC)
    gameFile.InitialVariables.append(VisTestConditionD)
    gameFile.InitialVariables.append(VisTestConditionE)
    gameFile.InitialVariables.append(VisTestConditionF)
    gameFile.InitialVariables.append(VisTestConditionGroupA)
    gameFile.InitialVariables.append(VisTestConditionGroupB)
    gameFile.InitialVariables.append(VisTestConditionGroupC)
    gameFile.InitialVariables.append(VisTestConditionGroupD)
    gameFile.InitialVariables.append(VisTestConditionGroupE)


    OpExample_VariableCollection = rpmaster_game_file_collection.Collection(
        'OpExample_VariableCollection', rpmaster_game_file_collection.CollectionTypes.VARIABLES)

    element1 = rpmaster_game_file_collection.Element(OpExampleVariable1.Name)
    element2 = rpmaster_game_file_collection.Element(OpExampleVariable2.Name)
    element3 = rpmaster_game_file_collection.Element(OpExampleVariable3.Name)

    OpExample_VariableCollection.Elements.append(element1)
    OpExample_VariableCollection.Elements.append(element2)
    OpExample_VariableCollection.Elements.append(element3)

    gameFile.Collections.append(OpExample_VariableCollection)


    menuVariable1 = rpmaster_game_file_root_menu_variable.RootMenuVariable('OpExampleVariable1')
    menuVariable2 = rpmaster_game_file_root_menu_variable.RootMenuVariable('OpExampleVariable2')
    menuVariable3 = rpmaster_game_file_root_menu_variable.RootMenuVariable('OpExampleVariable3')
    menuVariable4 = rpmaster_game_file_root_menu_variable.RootMenuVariable('ExampleInt')

    menuVariable1.VariablesUsed.append('OpExampleVariable1')
    menuVariable1.Equation = 'OpExampleVariable1'

    menuVariable2.VariablesUsed.append('OpExampleVariable2')
    menuVariable2.Equation = 'OpExampleVariable2'

    menuVariable3.VariablesUsed.append('OpExampleVariable3')
    menuVariable3.Equation = 'OpExampleVariable3'

    menuVariable4.VariablesUsed.append('ExampleInt')
    menuVariable4.Equation = 'ExampleInt'
    
    menVar_Threshold1 = rpmaster_game_file_menu_variable_threshold.RootMenuVariableThreshold(
        'menVar_Threshold1', None, 5, rpmaster_settings.Colors.GREEN)
    menVar_Threshold2 = rpmaster_game_file_menu_variable_threshold.RootMenuVariableThreshold(
        'menVar_Threshold2', 5, None, rpmaster_settings.Colors.RED)

    menuVariable1.EvaluationThresholds.append(menVar_Threshold1)
    menuVariable1.EvaluationThresholds.append(menVar_Threshold2)

    menuVariable2.EvaluationThresholds.append(menVar_Threshold1)
    menuVariable2.EvaluationThresholds.append(menVar_Threshold2)

    menuVariable3.EvaluationThresholds.append(menVar_Threshold1)
    menuVariable3.EvaluationThresholds.append(menVar_Threshold2)

    menuVariable4.EvaluationThresholds.append(menVar_Threshold1)
    menuVariable4.EvaluationThresholds.append(menVar_Threshold2)

    gameFile.RootMenuVariables.append(menuVariable1)
    gameFile.RootMenuVariables.append(menuVariable2)
    gameFile.RootMenuVariables.append(menuVariable3)
    gameFile.RootMenuVariables.append(menuVariable4)

    return


def generateExampleMenuStatuses(gameFile):
    menuStatus1 = rpmaster_game_file_root_menu_status.RootMenuStatus('menuStatus1')
    menuStatus2 = rpmaster_game_file_root_menu_status.RootMenuStatus('menuStatus2')
    menuStatus3 = rpmaster_game_file_root_menu_status.RootMenuStatus('menuStatus3')

    menuStatus1.VariablesUsed.append('OpExampleVariable1')
    menuStatus1.Equation = 'OpExampleVariable1'

    menuStatus2.VariablesUsed.append('OpExampleVariable2')
    menuStatus2.Equation = 'OpExampleVariable2'

    menuStatus3.VariablesUsed.append('OpExampleVariable3')
    menuStatus3.Equation = 'OpExampleVariable3'


    menuStatus1.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold1', None, 1, '#OpExampleVariable1 is lower than 1', rpmaster_settings.Colors.GREEN))
    menuStatus1.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold2', 1, 2, '#OpExampleVariable1 is between 1 and 2', rpmaster_settings.Colors.YELLOW))
    menuStatus1.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold3', 2, None, '#OpExampleVariable1 is over 2', rpmaster_settings.Colors.RED))

    menuStatus2.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold1', None, 1, '#OpExampleVariable2 is lower than 1', rpmaster_settings.Colors.GREEN))
    menuStatus2.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold2', 1, 2, '#OpExampleVariable1 is between 1 and 2', rpmaster_settings.Colors.YELLOW))
    menuStatus2.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold3', 2, None, '#OpExampleVariable2 is over 2', rpmaster_settings.Colors.RED))

    menuStatus3.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold1', None, 1, '#OpExampleVariable3 is lower than 1', rpmaster_settings.Colors.GREEN))
    menuStatus3.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold2', 1, 2, '#OpExampleVariable3 is between 1 and 2', rpmaster_settings.Colors.YELLOW))
    menuStatus3.EvaluationThresholds.append(rpmaster_game_file_menu_status_threshold.RootMenuStatusThreshold(
        'menVar_Threshold3', 2, None, '', rpmaster_settings.Colors.RED))

    gameFile.RootMenuStatusMessages.append(menuStatus1)
    gameFile.RootMenuStatusMessages.append(menuStatus2)
    gameFile.RootMenuStatusMessages.append(menuStatus3)
    return

def generateExampleOperationA(gameFile):
    operationA = rpmaster_game_file_operation.Operation('ExampleOpA')
    operationA.PrettyName = 'Example operation A'
    operationA.Comments = """Random int between operation. Two action threshold. Either prints messages from an action collection or messages from a string collection"""
    operationA.Message = 'You have chosen: #ExampleOpA'

    operationA.Type = rpmaster_game_file_operation.OperationTypes.RANDOM_INT_BETWEEN
    operationA.Lower = 0
    operationA.Upper = 10

    opA_Action1 = rpmaster_game_file_operation_action.Action(
        'opA_Action1', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    opA_Action2 = rpmaster_game_file_operation_action.Action(
        'opA_Action2', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)

    opA_Action1.PrettyName = "Example action 1"
    opA_Action2.PrettyName = "Example action 2"

    opA_Action1.Message = "Example message action 1"
    opA_Action2.Message = "Example message action 2"

    gameFile.Actions.append(opA_Action1)
    gameFile.Actions.append(opA_Action2)

    opA_ActionCollection = rpmaster_game_file_collection.Collection(
        'opA_ActionCollection', rpmaster_game_file_collection.CollectionTypes.ACTIONS)

        
    element1 = rpmaster_game_file_collection.Element(opA_Action1.Name, 0.9)
    element2 = rpmaster_game_file_collection.Element(opA_Action2.Name, 2)

    opA_ActionCollection.Elements.append(element1)
    opA_ActionCollection.Elements.append(element2)

    gameFile.Collections.append(opA_ActionCollection)

    opA_Action3 = rpmaster_game_file_operation_action.Action(
        'opA_Action3', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    
    opA_Action3.PrettyName = 'Operation A - Action 3'

    opA_String1 = rpmaster_game_file_string.RPMasterString(
        'opA_String1', 'Example string 1')
    opA_String2 = rpmaster_game_file_string.RPMasterString(
        'opA_String2', 'Example string 2')

    gameFile.Strings.append(opA_String1)
    gameFile.Strings.append(opA_String2)

    opA_MessageCollection = rpmaster_game_file_collection.Collection(
        'opA_MessageCollection', rpmaster_game_file_collection.CollectionTypes.STRINGS)

    element1 = rpmaster_game_file_collection.Element(opA_String1.Name, 1)
    element2 = rpmaster_game_file_collection.Element(opA_String2.Name, 1)

    opA_MessageCollection.Elements.append(element1)
    opA_MessageCollection.Elements.append(element2)

    gameFile.Collections.append(opA_MessageCollection)

    opA_Action3.MessageStringCollection = opA_MessageCollection.Name

    gameFile.Actions.append(opA_Action3)

    opA_Threshold1 = rpmaster_game_file_operation_threshold.Threshold(
        'opA_Threshold1', None, 5, opA_ActionCollection.Name)
    opA_Threshold2 = rpmaster_game_file_operation_threshold.Threshold(
        'opA_Threshold2', 5, None, opA_Action3.Name)

    operationA.EvaluationThresholds.append(opA_Threshold1)
    operationA.EvaluationThresholds.append(opA_Threshold2)

    gameFile.Operations.append(operationA)

    return


def generateExampleOperationB(gameFile):
    operationB = rpmaster_game_file_operation.Operation('ExampleOpB')
    operationB.PrettyName = 'Example operation B'
    operationB.Comments = """Random float between operation. Two action threshold. Either changes variable from a collection, or triggers Example operation A"""

    operationB.Type = rpmaster_game_file_operation.OperationTypes.RANDOM_FLOAT_BETWEEN
    operationB.Lower = 0.0
    operationB.Upper = 1.0

    opB_Action1 = rpmaster_game_file_operation_action.Action(
        'opB_Action1', rpmaster_game_file_operation_action.ActionTypes.CHANGE_VARIABLE)

    opB_Action1.PrettyName = "Example change variable action"
    opB_Action1.Message = "Example change variable action message"

    varEquation = rpmaster_game_file_operation_action.Equation("opB_Action1_Equation")
    varEquation.TargetVariable = 'OpExample_VariableCollection'
    varEquation.VariablesUsed.append('OpExampleVariable1')
    varEquation.VariablesUsed.append('OpExampleVariable2')
    varEquation.VariablesUsed.append('OpExampleVariable3')
    varEquation.Equation = 'OpExampleVariable1 + OpExampleVariable2 + OpExampleVariable3'

    opB_Action1.VariableEquations.append(varEquation.Name)

    gameFile.ActionEquations.append(varEquation)

    gameFile.Actions.append(opB_Action1)

    opB_Action2 = rpmaster_game_file_operation_action.Action(
        'opB_Action2', rpmaster_game_file_operation_action.ActionTypes.TRIGGER_OPERATION)

    opB_Action2.PrettyName = "Example trigger operation action"
    opB_Action2.Message = "Example trigger operation action message"
    opB_Action2.TriggeredOperation = "ExampleOpA"

    gameFile.Actions.append(opB_Action2)

    opB_Threshold1 = rpmaster_game_file_operation_threshold.Threshold(
        'opB_Threshold1', None, 0.5, opB_Action1.Name)
    opB_Threshold2 = rpmaster_game_file_operation_threshold.Threshold(
        'opB_Threshold2', 0.5, None, opB_Action2.Name)

    operationB.EvaluationThresholds.append(opB_Threshold1)
    operationB.EvaluationThresholds.append(opB_Threshold2)

    gameFile.Operations.append(operationB)

    return


def generateExampleOperationC(gameFile):
    operationC = rpmaster_game_file_operation.Operation('ExampleOpC')
    operationC.PrettyName = 'Example operation C'
    operationC.Comments = """Random int between operation. Four action threshold. Prints a different message for each action"""

    operationC.Type = rpmaster_game_file_operation.OperationTypes.RANDOM_INT_BETWEEN
    operationC.Lower = 0
    operationC.Upper = 100

    opC_Action1 = rpmaster_game_file_operation_action.Action(
        'opC_Action1', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    opC_Action2 = rpmaster_game_file_operation_action.Action(
        'opC_Action2', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    opC_Action3 = rpmaster_game_file_operation_action.Action(
        'opC_Action3', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    opC_Action4 = rpmaster_game_file_operation_action.Action(
        'opC_Action4', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)

    opC_Action1.PrettyName = "Example 4 way threshold message action 1"
    opC_Action2.PrettyName = "Example 4 way threshold message action 2"
    opC_Action3.PrettyName = "Example 4 way threshold message action 3"
    opC_Action4.PrettyName = "Example 4 way threshold message action 4"

    opC_Action1.Message = "Example 4 way threshold message action 1 message"
    opC_Action2.Message = "Example 4 way threshold message action 2 message"
    opC_Action3.Message = "Example 4 way threshold message action 3 message"
    opC_Action4.Message = "Example 4 way threshold message action 4 message"

    gameFile.Actions.append(opC_Action1)
    gameFile.Actions.append(opC_Action2)
    gameFile.Actions.append(opC_Action3)
    gameFile.Actions.append(opC_Action4)

    opC_Threshold1 = rpmaster_game_file_operation_threshold.Threshold(
        'opC_Threshold1', None, 25, opC_Action1.Name)
    opC_Threshold2 = rpmaster_game_file_operation_threshold.Threshold(
        'opC_Threshold2', 25, 50, opC_Action2.Name)
    opC_Threshold3 = rpmaster_game_file_operation_threshold.Threshold(
        'opC_Threshold3', 50, 75, opC_Action3.Name)
    opC_Threshold4 = rpmaster_game_file_operation_threshold.Threshold(
        'opC_Threshold4', 75, None, opC_Action4.Name)

    operationC.EvaluationThresholds.append(opC_Threshold1)
    operationC.EvaluationThresholds.append(opC_Threshold2)
    operationC.EvaluationThresholds.append(opC_Threshold3)
    operationC.EvaluationThresholds.append(opC_Threshold4)

    gameFile.Operations.append(operationC)

    return


def generateExampleOperationD(gameFile):
    operationD = rpmaster_game_file_operation.Operation('ExampleOpD')
    operationD.PrettyName = 'Example operation D'
    operationD.Comments = """Equation operation. Two action threshold. Prints a different message for each action"""

    operationD.Type = rpmaster_game_file_operation.OperationTypes.EQUATION

    operationD.VariablesUsed.append('OpExampleVariable1')

    operationD.Equation = 'OpExampleVariable1 * 5'

    opD_Action1 = rpmaster_game_file_operation_action.Action(
        'opD_Action1', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    opD_Action1.PrettyName = "Operation D Action 1"
    opD_Action1.Message = "#OpExampleVariable1 is lower than 2: &OpExampleVariable1"

    opD_Action2 = rpmaster_game_file_operation_action.Action(
        'opD_Action2', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    opD_Action2.PrettyName = "Operation D Action 2"
    opD_Action2.Message = "#OpExampleVariable1 is higher than 2: &OpExampleVariable1"

    gameFile.Actions.append(opD_Action1)
    gameFile.Actions.append(opD_Action2)

    opD_Threshold1 = rpmaster_game_file_operation_threshold.Threshold(
        'opD_Threshold1', None, 10, opD_Action1.Name)
    opD_Threshold2 = rpmaster_game_file_operation_threshold.Threshold(
        'opD_Threshold2', 10, None, opD_Action2.Name)

    operationD.EvaluationThresholds.append(opD_Threshold1)
    operationD.EvaluationThresholds.append(opD_Threshold2)

    gameFile.Operations.append(operationD)
    return

def generateExampleConditions(gameFile):
    conditionA = rpmaster_game_file_condition.Condition('conditionA', rpmaster_game_file_condition.ConditionTypes.VARIABLE_A_MATCH_VARIABLE_B)
    conditionA.Comments = 'Example condition A. Tests if CoExampleVariable1 and CoExampleVariable2 match. Should always return true if they have not been changed.'
    conditionA.VariableA = 'CoExampleVariable1'
    conditionA.VariableB = 'CoExampleVariable2'

    gameFile.Conditions.append(conditionA)

    conditionB = rpmaster_game_file_condition.Condition('conditionB', rpmaster_game_file_condition.ConditionTypes.VARIABLE_A_MATCH_VARIABLE_B)
    conditionB.Comments = 'Example condition B. Tests if CoExampleVariable1 and CoExampleVariable3 match. Should always return false if they have not been changed.'
    conditionB.VariableA = 'CoExampleVariable1'
    conditionB.VariableB = 'CoExampleVariable3'

    gameFile.Conditions.append(conditionB)



    conditionC = rpmaster_game_file_condition.Condition('conditionC', rpmaster_game_file_condition.ConditionTypes.VARIABLE_A_MATCH_VALUE)
    conditionC.Comments = 'Example condition C. Tests if CoExampleVariable1 matches "34". Should always return true if CoExampleVariable1 has not been changed.'
    conditionC.VariableA = 'CoExampleVariable1'
    conditionC.Value = 34

    gameFile.Conditions.append(conditionC)



    conditionD = rpmaster_game_file_condition.Condition('conditionD', rpmaster_game_file_condition.ConditionTypes.EQUATION_ABOVE_VALUE)
    conditionD.Comments = 'Example condition D. Tests if CoExampleVariable3 is over 34. Should always return true if CoExampleVariable3 has not been changed.'
    conditionD.Value = 34
    conditionD.Equation = 'CoExampleVariable3'
    conditionD.VariablesUsed.append('CoExampleVariable3')

    gameFile.Conditions.append(conditionD)

    conditionE = rpmaster_game_file_condition.Condition('conditionE', rpmaster_game_file_condition.ConditionTypes.EQUATION_ABOVE_VALUE)
    conditionE.Comments = 'Example condition E. Tests if CoExampleVariable1 is over 35. Should always return false if CoExampleVariable1 has not been changed.'
    conditionE.Value = 35
    conditionE.Equation = 'CoExampleVariable1'
    conditionE.VariablesUsed.append('CoExampleVariable1')

    gameFile.Conditions.append(conditionE)



    conditionF = rpmaster_game_file_condition.Condition('conditionF', rpmaster_game_file_condition.ConditionTypes.EQUATION_BELOW_VALUE)
    conditionF.Comments = 'Example condition F. Tests if OpExampleVariable1 is under 10. Should return true initually, but then false if operation B is run enough times'
    conditionF.Value = 10
    conditionF.Equation = 'OpExampleVariable1'
    conditionF.VariablesUsed.append('OpExampleVariable1')

    gameFile.Conditions.append(conditionF)


    ConditionGroupA = rpmaster_game_file_condition.ConditionGroup('ConditionGroupA', rpmaster_game_file_condition.ConditionGroupTypes.AND)
    ConditionGroupA.Comments = 'Example condition group A. Groups conditionA and conditionC with an AND evaluation. Should return true.'
    ConditionGroupA.Conditions.append('conditionA')
    ConditionGroupA.Conditions.append('conditionC')

    gameFile.ConditionGroups.append(ConditionGroupA)

    ConditionGroupB = rpmaster_game_file_condition.ConditionGroup('ConditionGroupB', rpmaster_game_file_condition.ConditionGroupTypes.AND)
    ConditionGroupB.Comments = 'Example condition group B. Groups conditionA and conditionB with an AND evaluation. Should return false.'
    ConditionGroupB.Conditions.append('conditionA')
    ConditionGroupB.Conditions.append('conditionB')

    gameFile.ConditionGroups.append(ConditionGroupB)



    ConditionGroupC = rpmaster_game_file_condition.ConditionGroup('ConditionGroupC', rpmaster_game_file_condition.ConditionGroupTypes.OR)
    ConditionGroupC.Comments = 'Example condition group C. Groups conditionA and conditionC with an OR evaluation. Should return true.'
    ConditionGroupC.Conditions.append('conditionA')
    ConditionGroupC.Conditions.append('conditionC')

    gameFile.ConditionGroups.append(ConditionGroupC)

    ConditionGroupD = rpmaster_game_file_condition.ConditionGroup('ConditionGroupD', rpmaster_game_file_condition.ConditionGroupTypes.OR)
    ConditionGroupD.Comments = 'Example condition group D. Groups conditionA and conditionB with an OR evaluation. Should return true.'
    ConditionGroupD.Conditions.append('conditionA')
    ConditionGroupD.Conditions.append('conditionB')

    gameFile.ConditionGroups.append(ConditionGroupD)

    ConditionGroupE = rpmaster_game_file_condition.ConditionGroup('ConditionGroupE', rpmaster_game_file_condition.ConditionGroupTypes.OR)
    ConditionGroupE.Comments = 'Example condition group E. Groups conditionB and conditionE with an OR evaluation. Should return false.'
    ConditionGroupE.Conditions.append('conditionB')
    ConditionGroupE.Conditions.append('conditionE')

    gameFile.ConditionGroups.append(ConditionGroupE)

    return

def generateExampleEvents(gameFile):


    EventA = rpmaster_game_file_event.Event('EventA')

    EventA.PrettyName = 'Example event A'
    EventA.MenuDisplayMessage = 'Example event A'
    EventA.Comments = 'Single fire example event, comes in randon time during the day in the next 2 days.'

    gameFile.Events.append(EventA)

    EventTestOperationA = rpmaster_game_file_operation.Operation('EventTestOperationA')
    EventTestOperationA.PrettyName = 'Event test operation A - EventA'
    EventTestOperationA.Comments = """Event test operation. Triggers EventA"""

    EventTestOperationA.Type = rpmaster_game_file_operation.OperationTypes.EQUATION

    EventTestOperationA.Equation = '1'

    evtTestOpA_Action = rpmaster_game_file_operation_action.Action(
        'evtTestOpA_Action', rpmaster_game_file_operation_action.ActionTypes.QUEUE_EVENT)
    evtTestOpA_Action.PrettyName = "Event test operation A Action"
    evtTestOpA_Action.Message = "Action is queueing EventA"

    evtTestOpA_Action.EventToQueue = 'EventA'
    evtTestOpA_Action.EventQueueMinHoursFromNow = 1
    evtTestOpA_Action.EventQueueMaxHoursFromNow = 48
    evtTestOpA_Action.EventQueueBlockedHoursMin = '18:00'
    evtTestOpA_Action.EventQueueBlockedHoursMax = '06:00'

    gameFile.Actions.append(evtTestOpA_Action)

    evtTestOpA_Threshold = rpmaster_game_file_operation_threshold.Threshold(
        'evtTestOpA_Threshold', None, 10, evtTestOpA_Action.Name)

    EventTestOperationA.EvaluationThresholds.append(evtTestOpA_Threshold)

    gameFile.Operations.append(EventTestOperationA)





    EventB = rpmaster_game_file_event.Event('EventB')

    EventB.PrettyName = 'Example event B'
    EventB.MenuDisplayMessage = 'Example event B'
    EventB.Comments = 'Repeating example event, starts at 12:00 and repeats each day. On each run it increments OpExampleVariable3 by 10%'
    EventB.RepeatTimeHours = 24
    EventB.QuizGroups.append('QuizGroupA')
    EventB.TriggeredActions.append('IncrementOpExampleVariable3')

    gameFile.Events.append(EventB)


    EventTestOperationB = rpmaster_game_file_operation.Operation('EventTestOperationB')
    EventTestOperationB.PrettyName = 'Event test operation B - EventB'
    EventTestOperationB.Comments = """Event test operation. Triggers EventB"""

    EventTestOperationB.Type = rpmaster_game_file_operation.OperationTypes.EQUATION

    EventTestOperationB.Equation = '1'

    evtTestOpB_Action = rpmaster_game_file_operation_action.Action(
        'evtTestOpB_Action', rpmaster_game_file_operation_action.ActionTypes.QUEUE_EVENT)
    evtTestOpB_Action.PrettyName = "Event test operation B Action"
    evtTestOpB_Action.Message = "Action is queueing EventB"

    evtTestOpB_Action.EventToQueue = 'EventB'
    evtTestOpB_Action.EventQueueMinHoursFromNow = 0
    evtTestOpB_Action.EventQueueMaxHoursFromNow = 25 
    evtTestOpB_Action.EventQueueBlockedHoursMin = '12:00'
    evtTestOpB_Action.EventQueueBlockedHoursMax = None

    gameFile.Actions.append(evtTestOpB_Action)

    evtTestOpB_Threshold = rpmaster_game_file_operation_threshold.Threshold(
        'evtTestOpB_Threshold', None, 10, evtTestOpB_Action.Name)

    EventTestOperationB.EvaluationThresholds.append(evtTestOpB_Threshold)

    gameFile.Operations.append(EventTestOperationB)


    IncrementOpExampleVariable3 = rpmaster_game_file_operation_action.Action(
        'IncrementOpExampleVariable3', rpmaster_game_file_operation_action.ActionTypes.CHANGE_VARIABLE)
    IncrementOpExampleVariable3.PrettyName = "Increment Operation Example Variable 3"


    IncrementOpExampleVariable3.Message = "Example daily increment variable action message"

    varEquation = rpmaster_game_file_operation_action.Equation("IncrementOpExampleVariable3_Equation")
    varEquation.TargetVariable = 'OpExampleVariable3'
    varEquation.VariablesUsed.append('OpExampleVariable3')
    varEquation.Equation = 'OpExampleVariable3 * 1.1'

    IncrementOpExampleVariable3.VariableEquations.append(varEquation.Name)

    gameFile.ActionEquations.append(varEquation)

    gameFile.Actions.append(IncrementOpExampleVariable3)
    return

def generateExampleQuizes(gameFile):
    QuizGroupA = rpmaster_game_file_quiz.QuizGroup('QuizGroupA', 'Example Quiz Group A')
    QuizGroupA.QuizMessage = 'You are answering questions for Example Quiz Group A'
    QuizGroupA.Questions.append('QuizQuestionA')
    gameFile.QuizGroups.append(QuizGroupA)

    QuizGroupB = rpmaster_game_file_quiz.QuizGroup('QuizGroupB', 'Example Quiz Group B')
    QuizGroupB.QuizMessage = 'You are answering questions for Example Quiz Group B'
    QuizGroupB.Questions.append('QuizQuestionA')
    QuizGroupB.Questions.append('QuizQuestionB')
    gameFile.QuizGroups.append(QuizGroupB)

    QuizQuestionA = rpmaster_game_file_quiz.QuizQuestion('QuizQuestionA', 'Example Quiz Question A')
    QuizQuestionA.Answers.append('QuizAnswer1')
    QuizQuestionA.Answers.append('QuizAnswer2')
    gameFile.QuizQuestions.append(QuizQuestionA)

    QuizQuestionB = rpmaster_game_file_quiz.QuizQuestion('QuizQuestionB', 'Example Quiz Question B')
    QuizQuestionB.Answers.append('QuizAnswer3')
    QuizQuestionB.Answers.append('QuizAnswer4')
    gameFile.QuizQuestions.append(QuizQuestionB)



    QuizAnswer1 = rpmaster_game_file_quiz.QuizAnswer('QuizAnswer1', 'Example Quiz Answer 1')
    QuizAnswer1.TriggeredActions.append('qzAns1_Action')
    gameFile.QuizAnswers.append(QuizAnswer1)

    qzAns1_Action = rpmaster_game_file_operation_action.Action(
        'qzAns1_Action', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    qzAns1_Action.PrettyName = "Quiz Answer 1 Action"
    qzAns1_Action.Message = "You picked Example Quiz Answer 1"
    gameFile.Actions.append(qzAns1_Action)

    QuizAnswer2 = rpmaster_game_file_quiz.QuizAnswer('QuizAnswer2', 'Example Quiz Answer 2')
    QuizAnswer2.TriggeredActions.append('qzAns2_Action')
    gameFile.QuizAnswers.append(QuizAnswer2)

    qzAns2_Action = rpmaster_game_file_operation_action.Action(
        'qzAns2_Action', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    qzAns2_Action.PrettyName = "Quiz Answer 2 Action"
    qzAns2_Action.Message = "You picked Example Quiz Answer 2"
    gameFile.Actions.append(qzAns2_Action)



    QuizAnswer3 = rpmaster_game_file_quiz.QuizAnswer('QuizAnswer3', 'Example Quiz Answer 3')
    QuizAnswer3.TriggeredActions.append('qzAns3_Action')
    gameFile.QuizAnswers.append(QuizAnswer3)

    qzAns3_Action = rpmaster_game_file_operation_action.Action(
        'qzAns3_Action', rpmaster_game_file_operation_action.ActionTypes.PRINT_MESSAGE_ONLY)
    qzAns3_Action.PrettyName = "Quiz Answer 3 Action"
    qzAns3_Action.Message = "You picked Example Quiz Answer 3"
    gameFile.Actions.append(qzAns3_Action)

    QuizAnswer4 = rpmaster_game_file_quiz.QuizAnswer('QuizAnswer4', 'Example Quiz Answer 4')
    QuizAnswer4.TriggeredActions.append('qzAns4_Action')
    gameFile.QuizAnswers.append(QuizAnswer4)

    qzAns4_Action = rpmaster_game_file_operation_action.Action(
        'qzAns4_Action', rpmaster_game_file_operation_action.ActionTypes.QUIZ_QUESTION)
    qzAns4_Action.PrettyName = "Quiz Answer 4 Action"
    qzAns4_Action.Message = "You picked Example Quiz Answer 4"
    qzAns4_Action.Questions.append('QuizQuestionA')
    gameFile.Actions.append(qzAns4_Action)


    QuizOperationA = rpmaster_game_file_operation.Operation('QuizOperationA')
    QuizOperationA.PrettyName = 'Quiz test operation A'
    QuizOperationA.Comments = """Quiz test operation. Triggers nothing itself, only the quiz answer actions."""
    QuizOperationA.QuizGroups.append('QuizGroupB')
    QuizOperationA.Type = rpmaster_game_file_operation.OperationTypes.EQUATION
    QuizOperationA.Equation = '1'
    
    quizOpA_Threshold = rpmaster_game_file_operation_threshold.Threshold('quizOpA_Threshold', None, 10, 'DoNothingAction')
    QuizOperationA.EvaluationThresholds.append(quizOpA_Threshold)

    gameFile.Operations.append(QuizOperationA)



    DoNothingAction = rpmaster_game_file_operation_action.Action(
        'DoNothingAction', rpmaster_game_file_operation_action.ActionTypes.DO_NOTHING)
    DoNothingAction.PrettyName = "Do Nothing Action"
    gameFile.Actions.append(DoNothingAction)

    return
