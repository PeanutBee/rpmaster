import yaml
import io
import os

import random
import datetime
from os import path
from enum import Enum

from asteval import Interpreter

import rpmaster_log
import rpmaster_settings
import rpmaster_save
import rpmaster_game_file_variable

def is_time_enabled():
    result = rpmaster_save.main_state().GameFile.EnableTime

    return result

def get_current_datetime():
    # rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Attempting to retreive current game datetime")

    if (is_time_enabled() == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "(get_current_datetime) Failed to retreive current game time as time has not been enabled in the game file")
        return -1


    result = -1
    varRef = rpmaster_save.main_state().get_variable_ref('CurrentGameDateTime', printLog = False)


    if varRef != -1:
        result = varRef.get_variable_value()
    else:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Failed to retreive current game time")

    # rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found datetime: " + str(result))
    return result

def get_current_date(rawDate = False):
    # rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Attempting to retreive current game date")
    if (is_time_enabled() == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "(get_current_date) Failed to retreive current game time as time has not been enabled in the game file")
        return -1

    datetime = get_current_datetime()

    date = -1

    if datetime != -1:
        start = rpmaster_save.main_state().GameFile.StartTime

        delta = datetime - start

        if rawDate == False:
            date = delta.days + 1 # For now it's easier to ignore weeks and months since they vary by game, +1 so it doesn't start at 0
        else:
            date = delta.days

    else:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Failed to retreive current game time")

    return date


def get_current_time():
    # rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Attempting to retreive current game time")
    if (is_time_enabled() == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "(get_current_time) Failed to retreive current game time as time has not been enabled in the game file")
        return -1

    datetime = get_current_datetime()

    time = -1

    if datetime != -1:
        time = datetime.time()   
    else:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Failed to retreive current game time")

    return time

def get_date(datetime, rawDate = False): # function to let you compare a datetime's day number with the current date

    start = rpmaster_save.main_state().GameFile.StartTime

    delta = datetime - start

    if rawDate == False:
        date = delta.days + 1 # For now it's easier to ignore weeks and months since they vary by game, +1 so it doesn't start at 0
    else:
        date = delta.days

    return date

def get_time(datetime): # Simple function, but keeps things consistent with get_date
    return datetime.time()  

def is_datetime_today(datetime):
    if (is_time_enabled() == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "(is_datetime_today) Failed to retreive current game time as time has not been enabled in the game file")
        return -1

    currentDate = get_current_date(True)
    comparisonDate = get_date(datetime, True)
    result = False

    if currentDate == comparisonDate:
        result = True
    else:
        result = False

    if comparisonDate < currentDate:
        rpmaster_log.debug_log(rpmaster_log.Level.WARNING, "Asked to check if a datetime date matches current date, but it is in the past")

    return result

def advance_current_datetime(newDatetime):
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Advancing time to: " + str(newDatetime))
    if (is_time_enabled() == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "(advance_current_datetime) Failed to retreive current game time as time has not been enabled in the game file")
        return -1

    datetime = get_current_datetime()

    if newDatetime > datetime:
        varRef = rpmaster_save.main_state().get_variable_ref('CurrentGameDateTime', printLog = False)
        varRef.set_variable_value(newDatetime)
        pass
    else:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "New game time is not later than current time: " + str(newDatetime) + ' / ' + str(datetime))
        pass

    return 

def is_between(time, time_range):
    if time_range[1] < time_range[0]:
        return time >= time_range[0] or time <= time_range[1]
    return time_range[0] <= time <= time_range[1]