import yaml
import io
import os
from os import path
from enum import Enum

import rpmaster_settings
import rpmaster_log
import rpmaster_game_file_variable
import rpmaster_save


defaultName = 'invalid'
defaultAction = 'invalid'


# only standard YAML tag

class Threshold(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileOperationThreshold'
    Name = ''
    Message = ''
    LowerBound = None
    UpperBound = None
    ActionName = ''

    def __init__(self, Name, LowerBound, UpperBound, ActionName, ):
        self.Name = Name
        self.Message = ''
        self.LowerBound = LowerBound
        self.UpperBound = UpperBound
        self.ActionName = ActionName

    def __repr__(self):
        return "%s(Name=%r, Message=%r, LowerBound=%r, UpperBound=%r, ActionName=%r, )" % (
            self.__class__.__name__, self.Name, self.Message, self.LowerBound, self.UpperBound, self.ActionName, )

    def is_valid_threshold(self):
        result = True
        
        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold has no name")
            return result

        if self.LowerBound == self.UpperBound:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Lower and upper threshold bounds are the same: " + self.Name)
            return result

        if (self.LowerBound != None) and (self.UpperBound != None):
            if self.LowerBound > self.UpperBound:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold lower bound is higher than upper bound: " + self.Name)
                return result
        
        if rpmaster_save.main_state().get_action_ref(self.ActionName, printLog = False) == -1:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find target action " + self.ActionName + " for: " + self.Name)

        return result
