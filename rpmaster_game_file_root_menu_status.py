import yaml
import io
import os
from os import path
from enum import Enum


import rpmaster_settings
import rpmaster_log
import rpmaster_game_file_equation
import rpmaster_save
import rpmaster_game_file_menu_status_threshold

# only standard YAML tag

class RootMenuStatus(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileRootMenuStatus'
    Name = ''
    Equation = ''
    VariablesUsed = [] # array of name references
    EvaluationThresholds = []  # thresholds hold the action to take reference

    def __init__(self, Name,):
        self.Name = Name
        self.Equation = ''
        self.VariablesUsed = []
        self.EvaluationThresholds = []

    def __repr__(self):
        return "%s(Name=%r, Equation=%r, VariablesUsed=%r, EvaluationThresholds=%r, )" % (
            self.__class__.__name__, self.Name, self.Equation, self.VariablesUsed, self.EvaluationThresholds, )

    def is_valid_status(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Root menu status has no name")
            return result

        if(rpmaster_game_file_equation.is_safeish_equation(self.Equation) == False):
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Invalid equation found: " + self.Name)
            
        for variable in self.VariablesUsed:
            if rpmaster_save.main_state().get_variable_ref(variable, True, False, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find used variable " + variable + " in: " + self.Name)
            pass

        for threshold in self.EvaluationThresholds:
            if threshold.is_valid_threshold() == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find used variable " + threshold.Name + " in: " + self.Name)
            pass

        return result

    def check_threshold(self, value):
        message = ''
        foundResult = False

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating " + str(len(self.EvaluationThresholds)) + " thresholds against value " + str(value))

        for threshold in self.EvaluationThresholds:
            if threshold.UpperBound != None:
                if value < threshold.UpperBound:
                    if threshold.LowerBound == None:
                        if len(threshold.Message) > 0: # Quick hack so that we don't add color tags to an empty message
                            message = rpmaster_log.colour_refs_and_vals(threshold.Message, threshold.Color)[0]
                        foundResult = True
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
                        break
                    elif value >= threshold.LowerBound:
                        if len(threshold.Message) > 0:
                            message = rpmaster_log.colour_refs_and_vals(threshold.Message, threshold.Color)[0]
                        foundResult = True
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
                        break
                    # else:
                    #     message = 'invalid'
                    #     rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold test value below hard lower limit")
                    #     foundResult = True
                    #     break
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Passed threshold: " + threshold.Name)

        if foundResult == False:
            if threshold.UpperBound == None:
                if len(threshold.Message) > 0:
                    message = rpmaster_log.colour_refs_and_vals(threshold.Message, threshold.Color)[0]
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
            else:
                message = 'invalid'
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold test did not find a valid match: " + str(value))

        return message

def get_status_message(menuStatus):
    message = None

    if(menuStatus.is_valid_status() == False):
        return message

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Generating status message: " + menuStatus.Name)

    equationResult = rpmaster_game_file_equation.evaluate(menuStatus.VariablesUsed, menuStatus.Equation)

    message = menuStatus.check_threshold(equationResult)

    return message