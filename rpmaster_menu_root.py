from __future__ import print_function, unicode_literals
import regex
import sys
import os
from os import system, name 
import psutil
import logging
import datetime

from pprint import pprint
from clint.textui import puts, indent, colored
from clint.textui import columns

from PyInquirer import style_from_dict, Token, prompt, print_json, Separator, color_print
from PyInquirer import Validator, ValidationError


import rpmaster_save
import rpmaster_settings
import rpmaster_log
import rpmaster_time
import rpmaster_game_file_variable
import rpmaster_game_file_operation
import rpmaster_menu_history
import rpmaster_menu_status
import rpmaster_menu_event
import rpmaster_menu_operations
import rpmaster_game_file_event
import rpmaster_menu_rollback
import rpmaster_game_file_root_menu_variable
import rpmaster_game_file_root_menu_status

from rpmaster_utilities import rpm_custom_style


def get_menu_choices(answers):
    options = []

    options.append(Separator('What would you like to do?'))

    options.append('History')

    if rpmaster_time.is_time_enabled():

        nextEventRef = rpmaster_game_file_event.get_next_event()
        if nextEventRef != 0 and nextEventRef != -1: # If there's no queued event (result 0) then no point in doing more
            nextEventTime = rpmaster_game_file_event.get_next_event_time()

            if nextEventRef != -1 and nextEventTime != -1:
                if rpmaster_time.is_datetime_today(nextEventTime) == True:
                    eventTimeString = nextEventTime.strftime("%H:%M")
                else:
                    eventTimeString = 'Day ' + str(rpmaster_time.get_date(nextEventTime)) + nextEventTime.strftime(" %H:%M")

                eventString = 'Next event: (' + eventTimeString + ') ' + nextEventRef.MenuDisplayMessage

                options.append(eventString)
            else:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Next event reference: " + str(nextEventRef))
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Next event time: " + str(nextEventTime))
            pass
        else:
            eventString = '! ERROR: No queued events found, something is wrong !' # This obviously needs to change if a good reason to have no queued events is found
            options.append(eventString)
            pass
        pass

    options.append('Status')
    options.append('Operations')
    options.append('Undo')
    options.append(Separator())
    options.append('Save')
    options.append(Separator())
    options.append('Exit')

    return options

def menu_root():
    clear()
    header_display()

    menu_root_choices = [
        {
            'type': 'list',
            'name': 'menu_option',
            'message': 'Role-Play Master Main Menu',
            'choices': get_menu_choices,
        },
    ]

    answers = prompt(menu_root_choices, style=rpm_custom_style)

    command = answers.get('menu_option', '')

    if(command.startswith('Next event: (') == True):
        clear()
        rpmaster_menu_event.menu_event()

    if(command == 'History'):
        clear()
        rpmaster_menu_history.menu_history()

    elif(command == 'Status'):
        clear()
        rpmaster_menu_status.menu_status()

    elif(command == 'Operations'):
        clear()
        rpmaster_menu_operations.menu_operations()

    elif(command == 'Undo'):
        clear()
        rpmaster_menu_rollback.menu_rollback()

    elif(command == 'Save'):
        clear()
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Manual save")
        rpmaster_save.save_state()

    elif(command == 'Exit'):
        if(ask_confirmation()):
            menu_exit()

    elif(command == 'Reload (restart)'):
        restart_program()

    return


def ask_confirmation():
    menu_confirm_choices = [
        {
            'type': 'list',
            'message': 'Are you sure?',
            'name': 'confirmation',
            'choices': [
                {
                    'name': 'No'
                },
                {
                    'name': 'Yes'
                },
            ]
        }
    ]
    answers = prompt(menu_confirm_choices, style=rpm_custom_style)
    return (answers.get('confirmation', '') == 'Yes')


def menu_exit():

    if(rpmaster_settings.loaded_settings().AutoSaveOnExit == True):
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "On exit auto save")
        rpmaster_save.save_state()

    clear()
    sys.exit(0)


def restart_program():
    """Restarts the current program, with file objects and descriptors
       cleanup
    """

    try:
        p = psutil.Process(os.getpid())
        for handler in p.open_files() + p.connections():
            os.close(handler.fd)
    except Exception as e:
        logging.error(e)

    python = sys.executable
    os.execl(python, python, *sys.argv)

def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
        
    title_bar()

def title_bar(): 
    #  ☺☻♥♦♫☼►◄↕‼¶§▬↨↑↓→←∟↔▲▼ !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~⌂ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜ¢£¥₧ƒáíóúñÑªº¿⌐¬½¼¡«»░▒▓│┤╡╢╖╕╣║╗╝╜╛┐└┴┬├─┼╞╟╚╔╩╦╠═╬╧╨╤╥╙╘╒╓╫╪┘┌█▄▌▐▀αßΓπΣσµτΦΘΩδ∞φε∩≡±≥≤⌠⌡÷≈°∙·√ⁿ²■ 

    green = getattr(colored, 'green')
    blue = getattr(colored, 'blue')
    cyan = getattr(colored, 'cyan')
    red = getattr(colored, 'red')

    title = 'Role-Play Master (' + rpmaster_settings.currentRpmVersion + ') - ' + rpmaster_save.main_state().Name

    if rpmaster_settings.loaded_settings().ShowDebugFunctions == False:
        decoration = '▼' * len(title)
    else:
        decoration = red('▼▼▼ DEBUG MODE ENABLED ▼▼▼')


    print(green(title))
    print(green(decoration))
    print('')

    if rpmaster_time.is_time_enabled() == True:
        date = rpmaster_time.get_current_date()
        time = rpmaster_time.get_current_time().strftime("%H:%M")

        print(green('♦ ') + cyan('Day ') + cyan(date) + green(' ─ ') + cyan(time) + green(' ♦'))
        pass

def header_display():
    print_header_variables()

    print_header_status_messages()

    return

def print_header_variables():
    variableColor = rpmaster_settings.variable_color()
    variableList = rpmaster_save.main_state().get_root_menu_variable_list()

    if(len(variableList) > 0):

        MajorColumnWidth = 40
        SeperatorColumnWidth = 1
        ColumnCount = 3

        horizontalMarker = '─'
        verticalMarkerMid = '│'
        verticalMarkerStart = '├'
        verticalMarkerMidCross = '┼'
        verticalMarkerEnd = '┤'
        horizontalLine = horizontalMarker * MajorColumnWidth

        fullHorizontalLineStart =   '┌' + horizontalLine + '┬' + horizontalLine + '┬' + horizontalLine + '┐'
        fullHorizontalLineMid =     '├' + horizontalLine + '┼' + horizontalLine + '┼' + horizontalLine + '┤'
        fullHorizontalLineEnd =     '└' + horizontalLine + '┴' + horizontalLine + '┴' + horizontalLine + '┘'


        puts(fullHorizontalLineStart)

        rowsCount = int((len(variableList) / ColumnCount) + (len(variableList) % ColumnCount))

        colVarRefs = []
        for row in range(rowsCount):
            for varIndex in range(ColumnCount):
                if((row * ColumnCount) + varIndex < len(variableList)):
                    menuVarRef = rpmaster_save.main_state().get_root_menu_variable_ref(variableList[(row * ColumnCount) + varIndex])
                    colVarRefs.append(menuVarRef)

        names = []
        values = []

        
        for colVar in colVarRefs:
            varName = colVar.VariableReference
            varRef = rpmaster_save.main_state().get_variable_ref(varName, True, False, printLog = False)
            names.append(varRef.PrettyName)


            if varRef.Type == rpmaster_game_file_variable.VariableTypes.FLOAT:
                value = format(varRef.get_variable_value(), '.2f')
                pass
            else:
                value = str(varRef.get_variable_value())
                pass


            value = rpmaster_game_file_root_menu_variable.get_variable_color(colVar)(value)
            values.append(value)
            pass

        
        for row in range(rowsCount):
            puts(
                verticalMarkerMid +
                # variableColor(get_next_var(names, (row * ColumnCount) + 0).center(MajorColumnWidth)) +
                (get_next_var(names, (row * ColumnCount) + 0).center(MajorColumnWidth)) +
                verticalMarkerMid +
                (get_next_var(names, (row * ColumnCount) + 1).center(MajorColumnWidth)) +
                verticalMarkerMid +
                (get_next_var(names, (row * ColumnCount) + 2).center(MajorColumnWidth)) +
                verticalMarkerMid)
                
            puts(
                verticalMarkerMid +
                (get_next_var(values, (row * ColumnCount) + 0).center(MajorColumnWidth)) +
                verticalMarkerMid +
                (get_next_var(values, (row * ColumnCount) + 1).center(MajorColumnWidth)) +
                verticalMarkerMid +
                (get_next_var(values, (row * ColumnCount) + 2).center(MajorColumnWidth)) +
                verticalMarkerMid)

            if row < rowsCount - 1:
                puts(fullHorizontalLineMid)
                pass


        puts(fullHorizontalLineEnd)
        pass

    return

def get_next_var(names, index):
    if index < len(names):
        return names[index]
    else:
        return ''
    return


def print_header_status_messages():
    statusList = rpmaster_save.main_state().get_root_menu_status_list()

    with indent(4, quote='.'):
        for status in statusList:
            statusRef = rpmaster_save.main_state().get_root_menu_status_ref(status)
            message = rpmaster_game_file_root_menu_status.get_status_message(statusRef)
            if len(message) > 0:
                puts(message)
                pass
            pass

    print('')

    return
