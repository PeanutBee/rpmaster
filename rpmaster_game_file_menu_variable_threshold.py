import yaml
import io
import os
from os import path
from enum import Enum

import rpmaster_settings
import rpmaster_log
import rpmaster_game_file_variable
import rpmaster_save


defaultName = 'invalid'
defaultColor = rpmaster_settings.Colors.WHITE

# only standard YAML tag

class RootMenuVariableThreshold(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileRootMenuVariableThreshold'
    Name = ''
    LowerBound = None
    UpperBound = None
    Color = ''

    def __init__(self, Name, LowerBound, UpperBound, Color=None, ):
        self.Name = Name
        self.LowerBound = LowerBound
        self.UpperBound = UpperBound
        
        if(Color is None):
            self.Color = defaultColor
        else:
            self.Color = Color

    def __repr__(self):
        return "%s(Name=%r, LowerBound=%r, UpperBound=%r, Color=%r, )" % (
            self.__class__.__name__, self.Name, self.LowerBound, self.UpperBound, self.Color, )

    def is_valid_threshold(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Root menu variable threshold has no name")
            return result

        if self.LowerBound == self.UpperBound:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Lower and upper threshold bounds are the same: " + self.Name)
            return result

        if (self.LowerBound != None) and (self.UpperBound != None):
            if self.LowerBound > self.UpperBound:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold lower bound is higher than upper bound: " + self.Name)
                return result

        colorTest = rpmaster_settings.get_color(self.Color)  

        if colorTest == None:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Root menu variable threshold has no color: " + self.Name)
            return result

        return result
