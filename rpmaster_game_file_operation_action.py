import yaml
import io
import os
import random
import datetime
from os import path
from enum import Enum

import rpmaster_settings
import rpmaster_log
import rpmaster_time
import rpmaster_save
import rpmaster_game_file_variable
import rpmaster_game_file_collection
import rpmaster_game_file_equation
import rpmaster_game_file_operation
import rpmaster_game_file_event
import rpmaster_game_file_condition
import rpmaster_game_file_quiz


class ActionTypes(Enum):
    INVALID = 'invalid'
    DO_NOTHING = 'do_nothing'
    PRINT_MESSAGE_ONLY = 'print_message_only'
    CHANGE_VARIABLE = 'change_variable'
    TRIGGER_OPERATION = 'trigger_operation'
    VARIABLE_PLUS_OPERATION = 'variable_plus_operation'
    QUEUE_EVENT = 'queue_event'
    QUIZ_QUESTION = 'quiz_question'


defaultName = 'invalid'
defaultPrettyName = 'New action'
defaultConditionComments = 'None'
defaultType = ActionTypes.INVALID
defaultMessage = ''


defaultEquationName = 'invalid'
defaultEquation = 'invalid'


# only standard YAML tag

class Action(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileAction'
    Name = ''
    PrettyName = ''
    Comments = ''
    Type = None
    Message = ''
    MessageStringCollection = ''
    # array  because each equation should only change one variable, and you may want to change more than one
    VariableEquations = []
    TriggeredOperation = ''

    EventToQueue = ''
    EventQueueMinHoursFromNow = None
    EventQueueMaxHoursFromNow = None
    EventQueueBlockedHoursMin = None
    EventQueueBlockedHoursMax = None

    Questions = []

    ExecutionConditions = []

    PreActions = []
    PostActions = []

    def __init__(self, Name, Type):
        self.Name = Name

        self.PrettyName = defaultPrettyName
        self.Comments = defaultConditionComments
        self.Type = Type
        self.Message = defaultMessage
        self.MessageStringCollection = ''
        self.VariableEquations = []
        self.TriggeredOperation = ''

        self.EventToQueue = ''
        self.EventQueueMinHoursFromNow = None
        self.EventQueueMaxHoursFromNow = None
        self.EventQueueBlockedHoursMin = None
        self.EventQueueBlockedHoursMax = None

        self.Questions = []

        self.ExecutionConditions = []

        self.PreActions = []
        self.PostActions = []

    def __repr__(self):
        return "%s(Name=%r, PrettyName=%r, Comments=%r, Type=%r, Message=%r, MessageStringCollection=%r, VariableEquations=%r, TriggeredOperation=%r, EventToQueue=%r, EventQueueMinHoursFromNow=%r, EventQueueMaxHoursFromNow=%r, EventQueueBlockedHoursMin=%r, EventQueueBlockedHoursMax=%r, Questions=%r, ExecutionConditions=%r, PreActions=%r, PostActions=%r, )" % (
            self.__class__.__name__, self.Name, self.PrettyName, self.Comments, self.Type, self.Message, self.MessageStringCollection, self.VariableEquations, self.TriggeredOperation, self.EventToQueue, self.EventQueueMinHoursFromNow, self.EventQueueMaxHoursFromNow, self.EventQueueBlockedHoursMin, self.EventQueueBlockedHoursMax, self.Questions, self.ExecutionConditions, self.PreActions, self.PostActions, )

    def is_valid_action(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Action has no name")
            return result

        if len(self.PrettyName) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Action has no pretty name: " + self.Name)
            return result

        if self.Type == ActionTypes.INVALID:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Action has invalid type: " + self.Name)
            return result

        if (self.Type == ActionTypes.TRIGGER_OPERATION) and (len(self.TriggeredOperation) == 0):
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Action has no operation defined: " + self.Name)
            return result

        if (self.Type == ActionTypes.TRIGGER_OPERATION) or (self.Type == ActionTypes.VARIABLE_PLUS_OPERATION):
            if rpmaster_save.main_state().get_operation_ref(self.TriggeredOperation, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find triggered operation " + self.TriggeredOperation + " for: " + self.Name)

        if (self.Type == ActionTypes.CHANGE_VARIABLE) or (self.Type == ActionTypes.VARIABLE_PLUS_OPERATION):
            for variableEquation in self.VariableEquations:
                if rpmaster_save.main_state().get_action_equation_ref(variableEquation, printLog = False) == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find variable equation " + variableEquation + " for: " + self.Name)

        
        if (self.Type == ActionTypes.QUEUE_EVENT):
            if rpmaster_game_file_event.is_valid_event_name(self.EventToQueue) == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Specified event to queue is not valid: " + self.EventToQueue)
                pass
            if self.EventQueueMinHoursFromNow > self.EventQueueMaxHoursFromNow:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Event hours minimum is higher than maximum: " + self.Name)
                return result

        if (self.Type == ActionTypes.QUIZ_QUESTION):
            for question in self.Questions:
                if rpmaster_save.main_state().get_quiz_question_ref(question, printLog = False) == False:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Reference to a question is not valid: " + question)
                    pass

        for action in self.PreActions:
            if rpmaster_save.main_state().get_action_ref(action, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find pre action " + action + " in: " + self.Name)

        for action in self.PostActions:
            if rpmaster_save.main_state().get_action_ref(action, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find post action " + action + " in: " + self.Name)

        return result
        
    def execute_action(self):

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Executing action: " + self.Name)

        if self.is_valid_action() == False:
            return

        for condition in self.ExecutionConditions:
            conRef = rpmaster_save.main_state().get_condition_ref(condition)
            if rpmaster_game_file_condition.evaluate_condition(conRef) == False:
                    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Action blocked by condition: " + self.Name  + " / " + conRef.Name)
                    return
            pass

        for preAction in self.PreActions:
            rpmaster_save.main_state().get_action_ref(preAction).execute_action()


        if self.Type != ActionTypes.DO_NOTHING:
            #Always print the message if there is one defined and this is not a do nothing action
            if self.MessageStringCollection != '':
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Printing action message from collection: " + self.MessageStringCollection)

                colRef = rpmaster_save.main_state().get_collection_ref(self.MessageStringCollection)
                message = colRef.get_value(True, False)

                rpmaster_log.game_log(message.String, False)
                pass
            elif self.Message != '':
                rpmaster_log.game_log(self.Message, False)
            pass

        if self.Type == ActionTypes.CHANGE_VARIABLE:
            self.change_variables()
            pass
        elif self.Type == ActionTypes.TRIGGER_OPERATION:
            self.trigger_operation()
            pass
        elif self.Type == ActionTypes.VARIABLE_PLUS_OPERATION:
            self.change_variables()
            self.trigger_operation()
            pass
        elif self.Type == ActionTypes.QUEUE_EVENT:
            validTime = False

            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Finding a time to queue event: " + self.EventToQueue)

            currentDateTime = rpmaster_time.get_current_datetime()
            startTime = rpmaster_save.main_state().GameFile.StartTime
            currentDate = startTime + datetime.timedelta(days=rpmaster_time.get_current_date(True)) 

            count = 0
            while validTime == False and count < 300: # Add some protection just in case it's impossible to solve
                count = count + 1


                if self.EventQueueBlockedHoursMin == None and self.EventQueueBlockedHoursMax == None: # Means an event that comes in at a random time
                    queueHours = random.randint(self.EventQueueMinHoursFromNow, self.EventQueueMaxHoursFromNow)
                    queueTime = datetime.timedelta(hours=queueHours)
                    triggerDateTime = currentDateTime + queueTime
                    triggerTime = rpmaster_time.get_time(triggerDateTime)

                    validTime = True
                    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found good time: " + str(triggerDateTime))
                
                elif self.EventQueueBlockedHoursMin != None and self.EventQueueBlockedHoursMax != None: # Means an event that comes in at a random time but with blocked hours
                    queueHours = random.randint(self.EventQueueMinHoursFromNow, self.EventQueueMaxHoursFromNow)
                    queueTime = datetime.timedelta(hours=queueHours)
                    triggerDateTime = currentDateTime + queueTime
                    triggerTime = rpmaster_time.get_time(triggerDateTime)

                    if (rpmaster_time.is_between(str(triggerTime), (str(self.EventQueueBlockedHoursMin), str(self.EventQueueBlockedHoursMax)))):
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Rejected time: " + str(triggerDateTime) + ' (within ' + str(self.EventQueueBlockedHoursMin) + ' to ' + str(self.EventQueueBlockedHoursMax) + ')')
                        pass
                    else:
                        validTime = True
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found good time: " + str(triggerDateTime) + ' (outside of ' + str(self.EventQueueBlockedHoursMin) + ' to ' + str(self.EventQueueBlockedHoursMax) + ')')
                        pass
                    pass
                
                elif self.EventQueueBlockedHoursMin != None and self.EventQueueBlockedHoursMax == None: # Means an event that comes at a specific time but on a random day
                    minDays = int(self.EventQueueMinHoursFromNow / 24)
                    maxDays = int(self.EventQueueMaxHoursFromNow / 24)

                    randomDay = random.randint(minDays, maxDays)


                    tempTime = datetime.datetime.strptime(self.EventQueueBlockedHoursMin, "%H:%M") 
                    triggerDelta = datetime.timedelta(days=randomDay, hours=tempTime.hour, minutes=tempTime.minute)

                    triggerDateTime = currentDate + triggerDelta

                    limitHours = datetime.timedelta(hours=self.EventQueueMaxHoursFromNow)
                    
                    if (triggerDateTime < currentDateTime + limitHours) and triggerDateTime > currentDateTime:
                        validTime = True
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found good time: " + str(triggerDateTime))
                        pass
                    else:
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Rejected time: " + str(triggerDateTime))
                        pass

                elif self.EventQueueBlockedHoursMin == None and self.EventQueueBlockedHoursMax != None: # Invalid arrangement
                    rpmaster_log.debug_log(rpmaster_log.Level.WARNING, "Invalid event queue instructions: EventQueueBlockedHoursMin is null while EventQueueBlockedHoursMax is set")

                pass

            if validTime == True:
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Queuing event: " + self.EventToQueue + ' / ' + str(triggerDateTime))

                rpmaster_game_file_event.add_event(self.EventToQueue, triggerDateTime)
                pass
            else:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Unable to find a valid time slot for event: " + self.EventToQueue)
                pass

            pass
        elif self.Type == ActionTypes.QUIZ_QUESTION:
            for question in self.Questions:
                rpmaster_game_file_quiz.execute_question(rpmaster_save.main_state().get_quiz_question_ref(question))
            pass
        
        for postAction in self.PostActions:
            rpmaster_save.main_state().get_action_ref(postAction).execute_action()

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Action complete: " + self.Name)

        return

    def change_variables(self):
        for varEquationName in self.VariableEquations:
            varEquationRef = rpmaster_save.main_state().get_action_equation_ref(varEquationName)
            if varEquationRef.TargetVariable != '':
                varRef = rpmaster_save.main_state().get_variable_ref(varEquationRef.TargetVariable)
                result = rpmaster_game_file_equation.evaluate(varEquationRef.VariablesUsed, varEquationRef.Equation)
                varRef.set_variable_value(result)
            else:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "No target variable for action equation: " + varEquationName)
            pass

        return

    def trigger_operation(self):
        if self.TriggeredOperation == '':
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "No trigger oprtation for action: " + self.Name)
        else:
            rpmaster_game_file_operation.execute_operation(rpmaster_save.main_state().get_operation_ref(self.TriggeredOperation))

        return

# only standard YAML tag

class Equation(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileActionEquation'
    Name = ''
    Equation = ''
    VariablesUsed = [] # array of name references
    TargetVariable = ''

    def __init__(self, Name):
        self.Name = Name
        self.Equation = defaultEquation
        self.VariablesUsed = []
        self.TargetVariable = ''

    def __repr__(self):
        return "%s(Name=%r, Equation=%r, VariablesUsed=%r, TargetVariable=%r, )" % (
            self.__class__.__name__, self.Name, self.Equation, self.VariablesUsed, self.TargetVariable, )

    def is_valid_equation(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Equation has no name")
            return result

        if(rpmaster_game_file_equation.is_safeish_equation(self.Equation) == False):
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Invalid equation found: " + self.Name)

        for variable in self.VariablesUsed:
            if rpmaster_save.main_state().get_variable_ref(variable, True, False, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find used variable " + variable + " in: " + self.Name)
            pass

        if rpmaster_save.main_state().get_variable_ref(self.TargetVariable, printLog = False) == -1:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find target variable " + self.TargetVariable + " for: " + self.Name)

        return result




