import yaml
import io
import os
from os import path
from enum import Enum

import random


import rpmaster_settings
import rpmaster_log
import rpmaster_game_file_variable
import rpmaster_save
import rpmaster_game_file_operation_action
import rpmaster_game_file_equation
import rpmaster_game_file_operation_threshold
import rpmaster_game_file_condition
import rpmaster_game_file_quiz


class OperationTypes(Enum):
    INVALID = 'invalid'
    RANDOM_INT_BETWEEN = 'random_int_between'
    RANDOM_FLOAT_BETWEEN = 'random_float_between'
    EQUATION = 'equation'


defaultOperationName = 'invalid'
defaultPrettyName = 'New operation'
defaultMessage = ''
defaultOperationComments = 'None'
defaultType = OperationTypes.INVALID
defaultEquation = 'invalid'
defaultBlockUndo = False


# only standard YAML tag

class Operation(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileOperation'
    Name = ''
    PrettyName = ''
    Comments = ''
    Message = ''
    Type = None
    BlockUndo = False

    AlwaysHideFromPlayer = False
    VisibilityConditions = []

    Lower = 0
    Upper = 0

    QuizGroups = []
    Equation = ''
    VariablesUsed = [] # array of name references
    EvaluationThresholds = []  # thresholds hold the action to take reference

    PreOperations = []
    PostOperations = []

    def __init__(self, Name):
        self.Name = Name

        self.PrettyName = defaultPrettyName
        self.Comments = defaultOperationComments
        # self.Message = 'You have chosen: #' + self.Name
        self.Message = defaultMessage
        self.Type = defaultType
        self.BlockUndo = defaultBlockUndo

        self.AlwaysHideFromPlayer = False
        self.VisibilityConditions = []

        self.Lower = 0
        self.Upper = 0

        self.QuizGroups = []
        self.Equation = defaultEquation
        self.VariablesUsed = []
        self.EvaluationThresholds = []

        self.PreOperations = []
        self.PostOperations = []

    def __repr__(self):
        return "%s(Name=%r, PrettyName=%r, Comments=%r, Message=%r, Type=%r, BlockUndo=%r, AlwaysHideFromPlayer=%r, VisibilityConditions=%r, Lower=%r, CommUpperents=%r, QuizGroups=%r, Equation=%r, VariablesUsed=%r, EvaluationThresholds=%r, PreOperations=%r, PostOperations=%r, )" % (
            self.__class__.__name__, self.Name, self.PrettyName, self.Comments, self.Message, self.Type, self.BlockUndo, self.AlwaysHideFromPlayer, self.VisibilityConditions, self.Lower, self.Upper, self.QuizGroups, self.Equation, self.VariablesUsed, self.EvaluationThresholds, self.PreOperations, self.PostOperations, )

    def is_valid_operation(self):
        result = True
        
        
        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Operation has no name")
            return result

        if len(self.PrettyName) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Operation has no pretty name")
            return result

        if self.Type == OperationTypes.INVALID:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Operation has invalid type: " + self.Name)
            return result

        if self.Type == OperationTypes.RANDOM_INT_BETWEEN or self.Type == OperationTypes.RANDOM_FLOAT_BETWEEN:
            if self.Lower >= self.Upper:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Operation lower value is higher than upper: " + self.Name)
                return result

        if (self.Type == OperationTypes.EQUATION) and (len(self.Equation) == 0):
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Operation has no equation defined: " + self.Name)
            return result

        for quizGroup in self.QuizGroups:
            if rpmaster_save.main_state().get_quiz_group_ref(quizGroup, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find operation quiz group " + quizGroup + " in: " + self.Name)

        if(rpmaster_game_file_equation.is_safeish_equation(self.Equation) == False):
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Invalid equation found: " + self.Name)

        for variable in self.VariablesUsed:
            if rpmaster_save.main_state().get_variable_ref(variable, True, False, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find used variable " + variable + " in: " + self.Name)
            pass

        for threshold in self.EvaluationThresholds:
            if threshold.is_valid_threshold() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found invalid threshold " + threshold.Name + " in: " + self.Name)
            pass

        for operation in self.PreOperations:
            if rpmaster_save.main_state().get_operation_ref(operation, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find pre operation " + operation + " in: " + self.Name)

        for operation in self.PostOperations:
            if rpmaster_save.main_state().get_operation_ref(operation, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find post operation " + operation + " in: " + self.Name)

        return result

    def is_visible(self):
        result = True
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Testing visibility for operation: " + self.Name)

        if rpmaster_settings.loaded_settings().ShowDebugFunctions == False: # Always show everything if we're in debug mode
            if(self.AlwaysHideFromPlayer == True):# Means that the always hide takes priority over the conditions
                result = False

            for condition in self.VisibilityConditions:
                conRef = rpmaster_save.main_state().get_condition_ref(condition)
                if rpmaster_game_file_condition.evaluate_condition(conRef) == False:
                    result = False
                    pass
                pass
        
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Tested visibility for operation: " + self.Name + ' - ' + str(result))

        return result

    def check_threshold(self, value):
        action = None
        foundResult = False

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating " + str(len(self.EvaluationThresholds)) + " thresholds against value " + str(value))

        for threshold in self.EvaluationThresholds:
            if threshold.UpperBound != None:
                if value < threshold.UpperBound:
                    if threshold.LowerBound == None:
                        action = threshold.ActionName
                        foundResult = True
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
                        break
                    elif value >= threshold.LowerBound:
                        action = threshold.ActionName
                        foundResult = True
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
                        break
                    # else:
                    #     action = 'invalid'
                    #     rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold test value below hard lower limit")
                    #     foundResult = True
                    #     break
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Passed threshold: " + threshold.Name)

        if foundResult == False:
            if threshold.UpperBound == None:
                action = threshold.ActionName
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
            else:
                action = 'invalid'
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold test did not find a valid match: " + str(value))

        return action


def execute_operation(operation):
    if(operation.is_valid_operation() == False):
        return

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Executing operation: " + operation.Name)
    rpmaster_log.game_log(operation.Message, False)

    if(operation.BlockUndo == True):
        rpmaster_save.main_state().delete_variables_history(operation.Name)

    for preOperation in operation.PreOperations:
        execute_operation(rpmaster_save.main_state().get_operation_ref(preOperation))

    if operation.Type == OperationTypes.RANDOM_INT_BETWEEN:
        value = random.randint(operation.Lower, operation.Upper)
        pass
    elif operation.Type == OperationTypes.RANDOM_FLOAT_BETWEEN:
        value = random.uniform(operation.Lower, operation.Upper)
        pass
    elif operation.Type == OperationTypes.EQUATION:
        for quizGroup in operation.QuizGroups:
            groupRef = rpmaster_save.main_state().get_quiz_group_ref(quizGroup)
            if groupRef != -1:
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Running pre equation quiz: " + groupRef.Name)
                rpmaster_game_file_quiz.execute_quiz_group(groupRef)
                pass
            pass

        value = rpmaster_game_file_equation.evaluate(operation.VariablesUsed, operation.Equation)
        pass

    actionName = operation.check_threshold(value)
    
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Looking for action:  " + actionName)

    actionRef = rpmaster_save.main_state().get_action_ref(actionName)

    if actionRef != -1:
        actionRef.execute_action()
        pass

    for postOperation in operation.PostOperations:
        execute_operation(rpmaster_save.main_state().get_operation_ref(postOperation))

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Operation complete: " + operation.Name)

    return
    