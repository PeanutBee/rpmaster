from __future__ import print_function, unicode_literals
import regex
import sys
import os
import psutil
import logging
from pprint import pprint

from PyInquirer import style_from_dict, Token, prompt, print_json, Separator, color_print
from PyInquirer import Validator, ValidationError

from rpmaster_utilities import rpm_custom_style

from clint.eng import join
from clint.textui import colored, indent, puts
from clint.textui import columns

import rpmaster_save
import rpmaster_time
import rpmaster_log
import rpmaster_game_file_operation


def get_menu_choices(answers):
    backups = rpmaster_save.main_state().get_var_history_list()

    options = []

    index = 0
    for backup in backups:
        operationRef = rpmaster_save.main_state().get_operation_ref(backup[0], False, False, printLog = False)
        eventRef = rpmaster_save.main_state().get_event_ref(backup[0], False, False, printLog = False)

        name = 'Error: event or operation not found'

        if(operationRef != -1):
            name = operationRef.PrettyName
            pass
        elif(eventRef != -1):
            name = eventRef.PrettyName
            pass
        else:
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Unable to find reference for history value: " + str(backup[0]))
            pass

        indexNo = str(index)
        indexNo = indexNo.zfill(2) 

        if rpmaster_time.is_time_enabled() == True:

            # print(green('♦ ') + blue('Day ') + blue(date) + green(' ─ ') + blue(time) + green(' ♦'))

            date = str(rpmaster_time.get_date(backup[1]))
            time = rpmaster_time.get_time(backup[1]).strftime(" %H:%M")

            options.append(indexNo + " | (" + 'Day ' + date + time + ") Restore state from before: " + name)
            pass
        else:
            options.append(indexNo + " | Restore state from before: " + name)
            pass

        index = index + 1 
        pass

    options.reverse() # So that the latest is at the top in the menu

    options.insert(0, "Return") # Simple safety mechanism to reduce chances of accidentally reverting
    options.insert(1, Separator())

    options.append(Separator())
    options.append('Return')
    return options




def menu_rollback():
    menu_rollback_choices = [
        {
            'type': 'list',
            'name': 'menu_option',
            'message': 'Which state would you like to rollback to? The history is shown from latest to oldest',
            'choices': get_menu_choices,
        },
    ]
    
    answers = prompt(menu_rollback_choices, style=rpm_custom_style)
    command = answers.get('menu_option', '')

    if(command == 'Return'):
        return

    menu_do_rollback(command)

    return


def menu_do_rollback(command):
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Performing undo action")


    index = int(command[0] + command[1]) # index is 2 characters

    rpmaster_save.main_state().load_variables_from_history(index)
    return
