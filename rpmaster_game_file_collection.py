import yaml
import io
import os

import random
from os import path
from enum import Enum

import rpmaster_save
import rpmaster_log
import rpmaster_game_file_variable
import rpmaster_game_file_operation
import rpmaster_game_file_operation_action
import rpmaster_game_file_equation
import rpmaster_game_file_string


class CollectionTypes(Enum):
    INVALID = 'invalid'
    VARIABLES = 'variables'
    OPERATIONS = 'operations'
    ACTIONS = 'actions'
    ACTION_EQUATIONS = 'action_equations'
    STRINGS = 'strings'
    EVENTS = 'events'
    QUIZ_GROUPS = 'quiz_groups'
    QUIZ_QUESTIONS = 'quiz_questions'
    QUIZ_ANSWERS = 'quiz_answers'
    COLLECTIONS = 'collections'


defaultName = 'invalid'
defaultType = CollectionTypes.INVALID
defaultTarget = 'invalid'
defaultCollectionComments = 'None'


# only standard YAML tag

class Collection(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileCollection'
    Name = ''
    Comments = ''
    Type = CollectionTypes.INVALID
    Elements = []

    def __init__(self, Name, Type):
        self.Name = Name
        self.Comments = defaultCollectionComments
        self.Type = Type
        self.Elements = []

    def __repr__(self):
        return "%s(Name=%r, Comments=%r, Type=%r, Elements=%r, )" % (
            self.__class__.__name__, self.Name, self.Comments, self.Type, self.Elements, )

    def is_valid_collection(self):
        result = True       

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Collection has no name")
            return result

        if len(self.Elements) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Empty collection: " + self.Name)
            return result

        if self.Type == CollectionTypes.INVALID:
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Invalid collection type: " + self.Name)
            pass
        elif self.Type == CollectionTypes.VARIABLES:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_variable_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find variable reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.OPERATIONS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_operation_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find operation reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.ACTIONS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_action_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find action reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.ACTION_EQUATIONS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_action_equation_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find action equation reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.STRINGS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_string_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find string reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.EVENTS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_event_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find event reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.QUIZ_GROUPS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_quiz_group_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find quiz group reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.QUIZ_QUESTIONS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_quiz_question_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find quiz question reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.QUIZ_ANSWERS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_quiz_answer_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find quiz answer reference in collection: " + self.Name + " - " + element.Target)
            pass
        elif self.Type == CollectionTypes.COLLECTIONS:
            for element in self.Elements:
                elementRef = rpmaster_save.main_state().get_collection_ref(element.Target, True, False, printLog = False)
                if elementRef == -1:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find collection reference in collection: " + self.Name + " - " + element.Target)
            pass

        for element in self.Elements:
            if element.is_valid_element() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found invalid element in collection: " + self.Name + " - " + element.Target)
                pass
            pass

        return result


    def get_value(self, ReturnRef, PrintLog=True):
        if self.is_valid_collection() == False:
            if ReturnRef == True:
                return -1
            else:
                return None

        name = ''
        result = None

        totalProbability = 0
        workingProbability = 0

        for element in self.Elements:
            totalProbability += element.Probability
            pass

        roll = random.uniform(0, totalProbability)

        if PrintLog == True:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Getting selection from collection: " + self.Name)
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Total probability = " + str(totalProbability))
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Roll = " + str(roll))

        for element in self.Elements:
            workingProbability += element.Probability
            if workingProbability >= roll:
                name = element.Target
                if PrintLog == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Landed on: " + name)
                break
            pass

        if ReturnRef == True:
            if (self.Type == CollectionTypes.VARIABLES):
                result = rpmaster_save.main_state().get_variable_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.OPERATIONS):
                result = rpmaster_save.main_state().get_operation_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.ACTIONS):
                result = rpmaster_save.main_state().get_action_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.ACTION_EQUATIONS):
                result = rpmaster_save.main_state().get_action_equation_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.STRINGS):
                result = rpmaster_save.main_state().get_string_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.EVENTS):
                result = rpmaster_save.main_state().get_event_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.QUIZ_GROUPS):
                result = rpmaster_save.main_state().get_quiz_group_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.QUIZ_QUESTIONS):
                result = rpmaster_save.main_state().get_quiz_question_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.QUIZ_ANSWERS):
                result = rpmaster_save.main_state().get_quiz_answer_ref(name, printLog = PrintLog)
            elif (self.Type == CollectionTypes.COLLECTIONS):
                result = rpmaster_save.main_state().get_collection_ref(name, printLog = PrintLog)

            if(result == None or result == -1):
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Failed to get valid object reference from collection: " + self.Name + ' / ' + name)
                return -1

        else:
            result = name
            if(result == None or result == -1):
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Failed to get valid object name from collection: " + self.Name + ' / ' + name)
                return -1

        return result

class Element(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileCollectionElement'
    Target = ''
    Probability = 1

    def __init__(self, Target, Probability=None):
        self.Target = Target

        if(Probability is None):
            self.Probability = 1
        else:
            self.Probability = Probability

    def __repr__(self):
        return "%s(Target=%r, Probability=%r, )" % (
            self.__class__.__name__, self.Target, self.Probability, )

    def is_valid_element(self):
        result = True       

        if len(self.Target) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Collection element has no target")

        if self.Probability < 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Collection element probability must be greater than 0")

        return result

