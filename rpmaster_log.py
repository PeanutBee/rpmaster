import logging
from enum import Enum
import inspect
import copy

from clint.textui import puts, indent, colored


import rpmaster_settings
import rpmaster_time
from rpmaster_settings import error_color
from rpmaster_settings import warning_color
from rpmaster_settings import time_color
from rpmaster_settings import variable_color
from rpmaster_settings import operation_color
from rpmaster_settings import action_color
from rpmaster_settings import quest_color
from rpmaster_settings import event_color
from rpmaster_settings import player_color
from rpmaster_settings import npc_color
import rpmaster_save
import rpmaster_game_file_variable

class Level(Enum):
    INFO = 'info'
    WARNING = 'warning'
    ERROR = 'error'
    CRITICAL = 'critical'

debugLogger = None
gameLogger = None

gameLogHistory = []

def setup_logger(name, log_file, formatter, level=logging.INFO):
    handler = logging.FileHandler(log_file)        
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

def initaliseDebugLog():
    global debugFormatter
    global debugLogger

    debugFormatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

    logFileDirectory = rpmaster_settings.loaded_settings().get_log_file_directory()

    debugLogger = setup_logger('debugLogger', logFileDirectory + 'RPM Debug.log', debugFormatter)

    return

def initaliseGameLog():
    global gameFormatter
    global gameLogger

    gameFormatter = logging.Formatter('%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M')

    logFileDirectory = rpmaster_settings.loaded_settings().get_log_file_directory()
    saveName = rpmaster_save.main_state().Name

    gameLogger = setup_logger('gameLogger', logFileDirectory + saveName + '.log', gameFormatter)

    return


def game_log(message, HoldAfterPrint=True):
    global gameLogger


    processedMessage = colour_refs_and_vals(message)
    
    message = processedMessage[0]
    cleanMessage = processedMessage[1]

    with indent(4, quote=colored.blue('>')):
        puts(message)

    gameLogHistory.append(message)

    if(gameLogger != None):
        gameLogger.info("(Day "+ str(rpmaster_time.get_current_date()) + " " + str(rpmaster_time.get_current_time()) +")" + cleanMessage)

    debug_log(Level.INFO, '(GAME LOG) ' + cleanMessage)

    if(HoldAfterPrint == True):
        input("...") # Causes the game log to hold until user progresses

    return
    
def get_game_log_history():
    return gameLogHistory

def debug_log(level, message):
    global debugLogger

    if(debugLogger == None):
        print(message)
        return

    logFileMessage = message
    
    # Fancy debugging, doesn't work in built exe
    
    # Find out which module the log is coming from
    # stack = inspect.stack()
    # previous_stack_frame = stack[1]
    # caller = inspect.getmodule(previous_stack_frame[0]).__name__

    # if(caller != 'rpmaster_log'): # Reduce clutter when we're just repeating a game log message
    #     logFileMessage = '(' + caller + '): ' + message
    # else:
    #     logFileMessage = '(game): ' + message


    if(level == Level.INFO):
        debugLogger.info(logFileMessage)
        if(rpmaster_settings.loaded_settings().PrintDebug == True):
            print(("INFO: " + message))
    elif(level == Level.WARNING):
        debugLogger.warning(logFileMessage)
        print(warning_color()("WARNING: " + message))
    elif(level == Level.ERROR):
        debugLogger.error(logFileMessage)
        print(error_color()("ERROR: " + message))
    elif(level == Level.CRITICAL):
        debugLogger.critical(logFileMessage)
        print(error_color()("CRITICAL ERROR: " + message))

    return


def colour_refs_and_vals(message, baseColor=None):
    refkey = '#'
    valKey = '&'
    space = ' '

    splitMessage = message.split(space)
    splitMessageClean = copy.copy(splitMessage)

    for index in range(0, len(splitMessage)):
        word = splitMessage[index]
        
        if len(word) > 1: # Skip if it's just a single instance of the key

            if word.startswith(refkey):
                word = word.lstrip(refkey)

                variable = False
                operation = False
                action = False
                event = False

                ref = rpmaster_save.main_state().get_variable_ref(word, False, False, printLog = False)

                if ref != -1:
                    variable = True
                    pass
                elif ref == -1:
                    ref = rpmaster_save.main_state().get_operation_ref(word, False, False, printLog = False)
                    pass

                if ref != -1 and variable == False:
                    operation = True
                    pass
                elif ref == -1:
                    ref = rpmaster_save.main_state().get_action_ref(word, False, False, printLog = False)
                    pass

                if ref != -1 and operation == False:
                    action = True
                    pass
                elif ref == -1:
                    ref = rpmaster_save.main_state().get_event_ref(word, False, False, printLog = False)
                    pass

                if ref != -1 and action == False:
                    event = True
                    pass
                elif ref == -1:
                    if baseColor != None:
                        splitMessage[index] = rpmaster_settings.get_color(baseColor)(word) # If we can't find the reference then just skip it
                        pass
                    pass

                if variable == True:
                    splitMessage[index] = variable_color()(ref.PrettyName)
                    splitMessageClean[index] = (ref.PrettyName)
                    pass
                elif operation == True:
                    splitMessage[index] = operation_color()(ref.PrettyName)
                    splitMessageClean[index] = (ref.PrettyName)
                    pass
                elif action == True:
                    splitMessage[index] = action_color()(ref.PrettyName)
                    splitMessageClean[index] = (ref.PrettyName)
                    pass
                elif event == True:
                    splitMessage[index] = event_color()(ref.PrettyName)
                    splitMessageClean[index] = (ref.PrettyName)
                    pass


            
            elif word.startswith(valKey):
                word = word.lstrip(valKey)

                varRef = rpmaster_save.main_state().get_variable_ref(word, False, False, printLog = False)
                if varRef != -1:
                    if varRef.Type == rpmaster_game_file_variable.VariableTypes.DATETIME:
                        splitMessage[index] = time_color()(str(varRef.get_variable_value().strftime("%Y-%m-%d %H:%M")))
                        splitMessageClean[index] = (str(varRef.get_variable_value()))
                        pass
                    else:
                        splitMessage[index] = variable_color()(str(varRef.get_variable_value()))
                        splitMessageClean[index] = (str(varRef.get_variable_value()))
                        pass

                    pass
                pass

            else:
                if baseColor != None:
                    splitMessage[index] = rpmaster_settings.get_color(baseColor)(word)
                    pass
                pass
        else:
            if baseColor != None:
                splitMessage[index] = rpmaster_settings.get_color(baseColor)(word)
                pass
            pass
        pass

    colorMessage = ''
    cleanMessage = ''

    for word in splitMessage: # Can't use join because of the colored strings
        colorMessage = colorMessage + word + space
        pass

    for word in splitMessageClean: # Can't use join because of the colored strings
        cleanMessage = cleanMessage + word + space
        pass

    colorMessage = colorMessage[:-1] # Trim the trailing space
    cleanMessage = cleanMessage[:-1] # Trim the trailing space

    return [colorMessage, cleanMessage]