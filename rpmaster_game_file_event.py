import yaml
import io
import os

import random
import datetime
from os import path
from enum import Enum

import rpmaster_settings
import rpmaster_save
import rpmaster_log
import rpmaster_time
import rpmaster_game_file_variable
import rpmaster_game_file_operation
import rpmaster_game_file_operation_action
import rpmaster_game_file_equation
import rpmaster_game_file_string
import rpmaster_game_file_event
import rpmaster_game_file_condition
import rpmaster_game_file_quiz

# class EventTypes(Enum):
#     INVALID = 'invalid'
#     TEST = 'test'


defaultName = 'invalid'
# defaultType = EventTypes.INVALID
defaultMenuDisplayMessage = 'New event'
defaultComments = 'None'
defaultRepeatTimeHours = 0


# only standard YAML tag

class Event(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileEvent'
    Name = ''
    PrettyName = ''
    MenuDisplayMessage = ''
    Comments = ''
    Message = ''
    
    # Type = EventTypes.INVALID
    RepeatTimeHours = 0

    BlockUndo = False
    EndOfDayEventSaveTrigger = False

    QuizGroups = []
    TriggeredActions = []
    PreOperations = []
    PostOperations = []

    ExecutionConditions = []

    def __init__(self, Name):
        self.Name = Name
        self.PrettyName = Name
        self.MenuDisplayMessage = defaultMenuDisplayMessage
        self.Comments = defaultComments

        self.Message =  'Event has occured: #' + self.Name
        # self.Type = Type
        self.RepeatTimeHours = defaultRepeatTimeHours

        self.BlockUndo = False
        self.EndOfDayEventSaveTrigger = False

        self.QuizGroups = []
        self.TriggeredActions = []
        self.PreOperations = []
        self.PostOperations = []

        self.ExecutionConditions = []

    def __repr__(self):
        return "%s(Name=%r, PrettyName=%r, MenuDisplayMessage=%r, Comments=%r, Message=%r, RepeatTimeHours=%r, BlockUndo=%r, EndOfDayEventSaveTrigger=%r, QuizGroups=%r, TriggeredActions=%r, PreOperations=%r, PostOperations=%r, PostOperations=%r, ExecutionConditions=%r, )" % (
            self.__class__.__name__, self.Name, self.PrettyName, self.MenuDisplayMessage, self.Comments, self.Message, self.RepeatTimeHours, self.BlockUndo, self.EndOfDayEventSaveTrigger, self.QuizGroups, self.TriggeredActions, self.PreOperations, self.PostOperations, self.PostOperations, self.ExecutionConditions, )

    def is_valid_event(self):
        result = True       

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Event has no name")
            return result

        if len(self.PrettyName) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Event has no pretty name")
            return result

        if len(self.MenuDisplayMessage) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Event has no menu display message")
            return result

        # if self.Type == EventTypes.INVALID:
        #     result = False
        #     rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Event has invalid type: " + self.Name)
        #     return result

        for quizGroup in self.QuizGroups:
            if rpmaster_save.main_state().get_quiz_group_ref(quizGroup, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find event quiz group " + quizGroup + " in: " + self.Name)

        for action in self.TriggeredActions:
            if rpmaster_save.main_state().get_action_ref(action, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find action " + action + " in: " + self.Name)

        for operation in self.PreOperations:
            if rpmaster_save.main_state().get_operation_ref(operation, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find pre operation " + operation + " in: " + self.Name)
                
        for operation in self.PostOperations:
            if rpmaster_save.main_state().get_operation_ref(operation, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find post operation " + operation + " in: " + self.Name)

        return result


class EventQueueElement(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileEventQueueElement'
    TargetEvent = ''
    Time = None

    def __init__(self, TargetEvent, Time):
        self.TargetEvent = TargetEvent
        self.Time = Time

    def __repr__(self):
        return "%s(TargetEvent=%r, Time=%r, )" % (
            self.__class__.__name__, self.TargetEvent, self.Time, )

    def is_valid_event_queue_element(self):
        result = True       

        eventRef = rpmaster_save.main_state().get_event_ref(self.TargetEvent, printLog = False)
            
        if eventRef == -1:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Event from queue was not found: " + self.TargetEvent)

        if eventRef.is_valid_event() == False:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Event from queue is not valid: " + self.TargetEvent)
    
        return result

class EventQueue(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileEventQueue'
    Queue = []

    def __init__(self):
        self.Queue = []

    def __repr__(self):
        return "%s(Queue=%r, )" % (
            self.__class__.__name__, self.Queue, )

    def is_valid_event_queue(self):
        result = True       

        for element in self.Queue:
            if element.is_valid_event_queue_element() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found invalid event in queue")
            pass
        return result


def get_event_queue():
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Getting event queue")

    if rpmaster_time.is_time_enabled() == False:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Cannot retreive event queue, time is not enabled")
        return -1

    queue = rpmaster_save.main_state().get_variable_ref('EventQueueVariable').Value


    if queue == -1:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Cannot retreive event queue, it was not found in variable list")
        return -1

    if(queue.is_valid_event_queue() == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Event queue was found to be corrupt")
        return -1

    return queue

def clear_old_events():
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Clearing old events")

    currentTime = rpmaster_time.get_current_datetime()
    eventQueue = get_event_queue()

    eventCount = len(eventQueue.Queue)
    for elementIndex in range(eventCount): # elementIndex is not uses as it becomes invalid if we pop
        if eventQueue.Queue[0].Time < currentTime:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Deleted event from queue: " + eventQueue.Queue[0].TargetEvent)
            eventQueue.Queue.pop(0)
            pass

    return

def clear_current_event():
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Clearing current event from queue")

    currentTime = rpmaster_time.get_current_datetime()
    eventQueue = get_event_queue()

    eventCount = len(eventQueue.Queue)

    for elementIndex in range(eventCount): # elementIndex is not uses as it becomes invalid if we pop
        if eventQueue.Queue[0].Time <= currentTime: # Only difference between this and the regular queue is the <=
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Deleted event from queue: " + eventQueue.Queue[0].TargetEvent)
            eventQueue.Queue.pop(0)
            pass

    return

def get_next_event():
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Getting next event")
    clear_old_events()
    eventQueue = get_event_queue()
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Queue size: " + str(len(eventQueue.Queue)))
    if len(eventQueue.Queue) > 0:
        eventElement = eventQueue.Queue[0]

        if eventElement.is_valid_event_queue_element() == False:
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Cannot retreive next event, event queue element is not valid")
            return -1
        
        eventRef = rpmaster_save.main_state().get_event_ref(eventElement.TargetEvent)
        
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found event: " + eventRef.Name)
        return eventRef
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Could not find any queued events")
    return 0  # Not an error, but not valid either

def get_next_event_time():
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Getting next event time")
    eventQueue = get_event_queue()
    clear_old_events()
    if len(eventQueue.Queue) > 0:
        eventElement = eventQueue.Queue[0]

        if eventElement.is_valid_event_queue_element() == False:
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Cannot retreive next event time, event queue element is not valid")
            return -1

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found event time: " + eventElement.TargetEvent + ' / ' + str(eventElement.Time))
        return eventElement.Time
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Could not find any queued events")
    return 0  # Not an error, but not valid either

def is_valid_event_name(name):
    result = False

    eventRef = rpmaster_save.main_state().get_event_ref(name, printLog = False)

    if eventRef != -1:
        result = True
        pass

    return result

def is_valid_event_time(time):
    result = False

    currentTime = rpmaster_time.get_current_datetime()

    if currentTime < time: # Make sure the event will be in the future
        result = True
        pass

    return result

def add_event(targetEvent, time):
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Adding new event to queue: " + targetEvent)
    
    eventRef = rpmaster_save.main_state().get_event_ref(targetEvent)

    if eventRef != -1:
        for condition in eventRef.ExecutionConditions:
            conRef = rpmaster_save.main_state().get_condition_ref(condition)
            if rpmaster_game_file_condition.evaluate_condition(conRef) == False:
                    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Event queuing blocked by condition: " + targetEvent  + " / " + conRef.Name)
                    return
            pass

    eventQueue = get_event_queue()
    clear_old_events()

    if ((is_valid_event_name(targetEvent) == True) and (is_valid_event_time(time) == True)):
        newQueueElement = EventQueueElement(targetEvent, time)

        foundSlot = False

        eventCount = len(eventQueue.Queue)
        for elementIndex in range(eventCount):
            if eventQueue.Queue[elementIndex].Time == newQueueElement.Time:
                newQueueElement.Time = newQueueElement.Time + datetime.timedelta(minutes=1)
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found a conflicting event in queue, so pushed new one back by one minute: " + eventQueue.Queue[elementIndex].TargetEvent + ' / ' + str(eventQueue.Queue[elementIndex].Time)) 
                pass
            if eventQueue.Queue[elementIndex].Time > newQueueElement.Time:
                eventQueue.Queue.insert(elementIndex, newQueueElement)
                foundSlot = True
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found event queue slot within queue: " + str(elementIndex)) 
                break
        pass

        if foundSlot == False: # If we've been through the whole queue and haven't found a slot then this must go at the end
            eventQueue.Queue.append(newQueueElement)
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found event queue slot at the end of queue: " + str(len(eventQueue.Queue)-1)) 
            pass
    elif(is_valid_event_name(targetEvent) == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Cannot add new event to queue, invalid name: " + targetEvent + ' / ' + str(time))
        pass
    elif(is_valid_event_time(time) == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Cannot add new event to queue, invalid time: " + targetEvent + ' / ' + str(time))
        pass
    return

def execute_event(event):
    if(event.is_valid_event() == False):
        return

    if(event.BlockUndo == True):
        rpmaster_save.main_state().delete_variables_history(event.Name)

    rpmaster_time.advance_current_datetime(get_next_event_time()) # Since we haven't advanced time yet, this is still technically the next event
    clear_current_event()

    for preOperation in event.PreOperations:
        rpmaster_game_file_operation.execute_operation(rpmaster_save.main_state().get_operation_ref(preOperation))


    if len(event.Message) != 0: # Lets the writer streamline events and quizes to save player time
        rpmaster_log.game_log(event.Message, False)


    for quizGroup in event.QuizGroups:
        groupRef = rpmaster_save.main_state().get_quiz_group_ref(quizGroup)
        if groupRef != -1:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Running event quiz: " + groupRef.Name)
            rpmaster_game_file_quiz.execute_quiz_group(groupRef)
            pass
        pass
    
    for action in event.TriggeredActions:
        actionRef = rpmaster_save.main_state().get_action_ref(action)
        if actionRef != -1:
            actionRef.execute_action()


    if event.RepeatTimeHours != 0:
        currentTime = rpmaster_time.get_current_datetime()

        nextOccurance = currentTime + datetime.timedelta(hours=event.RepeatTimeHours)
        
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Queueing repeat event: " + event.Name + ' / ' + str(nextOccurance))

        rpmaster_game_file_event.add_event(event.Name, nextOccurance)
        pass

    for postOperation in event.PostOperations:
        rpmaster_game_file_operation.execute_operation(rpmaster_save.main_state().get_operation_ref(postOperation))

    if event.EndOfDayEventSaveTrigger == True and rpmaster_settings.loaded_settings().AutoSaveEachDay == True:
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "End of day auto save after event: " + event.Name)
        rpmaster_save.save_state()
        pass

    return
