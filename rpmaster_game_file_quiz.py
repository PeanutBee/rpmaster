import yaml
import io
import os

import random
import datetime
from os import path
from enum import Enum

from PyInquirer import style_from_dict, Token, prompt
from PyInquirer import Validator, ValidationError
from PyInquirer import Separator

import rpmaster_settings
import rpmaster_save
import rpmaster_log
import rpmaster_time
import rpmaster_utilities
import rpmaster_game_file_variable
import rpmaster_game_file_operation
import rpmaster_game_file_operation_action
import rpmaster_game_file_equation
import rpmaster_game_file_string
import rpmaster_game_file_event
import rpmaster_game_file_operation_action
import rpmaster_game_file_condition

from rpmaster_utilities import rpm_custom_style


class QuizGroup(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileQuizGroup'
    Name = ''
    QuizString = ''
    QuizMessage = ''
    Questions = []
    VisibilityConditions = []

    def __init__(self, Name, QuizString, ):
        self.Name = Name
        self.QuizString = QuizString
        self.QuizMessage = QuizString

        self.Questions = []
        self.VisibilityConditions = []

    def __repr__(self):
        return "%s(Name=%r, QuizString=%r, QuizMessage=%r, Questions=%r, VisibilityConditions=%r, )" % (
            self.__class__.__name__, self.Name, self.QuizString, self.QuizMessage, self.Questions, self.VisibilityConditions, )

    def is_valid_quiz_group(self):
        result = True       

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Quiz group has no name")
            return result

        if len(self.QuizString) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Quiz group has no quiz string")
            return result

        for question in self.Questions:
            questionRef = rpmaster_save.main_state().get_quiz_question_ref(question, printLog = False)
            if questionRef == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find question " + question + " in: " + self.Name)
            elif questionRef.is_valid_quiz_question() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found invalid question " + question + " in quiz group: " + self.Name)
                pass

        return result


class QuizQuestion(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileQuizQuestion'
    Name = ''
    QuestionString = ''
    Answers = []
    VisibilityConditions = []


    def __init__(self, Name, QuestionString, ):
        self.Name = Name
        self.QuestionString = QuestionString

        self.Answers = []
        self.VisibilityConditions = []

    def __repr__(self):
        return "%s(Name=%r, QuestionString=%r, Answers=%r, VisibilityConditions=%r, )" % (
            self.__class__.__name__, self.Name, self.QuestionString, self.Answers, self.VisibilityConditions, )

    def is_valid_quiz_question(self):
        result = True       

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Quiz question has no name")
            return result

        if len(self.QuestionString) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Quiz question has no question string")
            return result

        for answer in self.Answers:
            answerRef = rpmaster_save.main_state().get_quiz_answer_ref(answer, printLog = False) 
            if answerRef == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find answer " + answer + " in: " + self.Name)
            elif answerRef.is_valid_quiz_answer() == False:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found invalid answer " + answer + " in quiz question: " + self.Name)
                pass

        return result



class QuizAnswer(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileQuizAnswer'
    Name = ''
    AnswerString = ''
    TriggeredActions = []
    TriggeredOperations = []
    VisibilityConditions = []

    def __init__(self, Name, AnswerString, ):
        self.Name = Name
        self.AnswerString = AnswerString
        self.TriggeredActions = []
        self.TriggeredOperations = []
        self.VisibilityConditions = []

    def __repr__(self):
        return "%s(Name=%r, AnswerString=%r, TriggeredActions=%r, TriggeredOperations=%r, VisibilityConditions=%r, )" % (
            self.__class__.__name__, self.Name, self.AnswerString, self.TriggeredActions, self.TriggeredOperations, self.VisibilityConditions, )

    def is_valid_quiz_answer(self):
        result = True       

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Quiz answer has no name")
            return result

        if len(self.AnswerString) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Quiz answer has no answer string")
            return result
            
        for action in self.TriggeredActions:
            if rpmaster_save.main_state().get_action_ref(action, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find action " + action + " in: " + self.Name)

        for operation in self.TriggeredOperations:
            if rpmaster_save.main_state().get_operation_ref(operation, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find operation " + operation + " in: " + self.Name)

        return result

def execute_quiz_group(quizGroup):
    if(quizGroup.is_valid_quiz_group() == False):
        return

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Running quiz group: " + quizGroup.Name)

    for condition in quizGroup.VisibilityConditions:
        conRef = rpmaster_save.main_state().get_condition_ref(condition)
        if rpmaster_game_file_condition.evaluate_condition(conRef) == False:
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Quiz group blocked by condition: " + quizGroup.Name  + " / " + conRef.Name)
                return
        pass

    if len(quizGroup.QuizMessage) != 0: # Lets the writer streamline events and quizes to save player time
        rpmaster_log.game_log(quizGroup.QuizMessage)

    for question in quizGroup.Questions:
        questionRef = rpmaster_save.main_state().get_quiz_question_ref(question) 
        if questionRef != -1:
            execute_question(questionRef)
        pass

    return


CurrentQuestion = None
choices = []
answerIdNames = []

def get_question_choices(answers):
    global CurrentQuestion
    global choices
    global answerIdNames

    answerIdNames = []
    choices = []
    
    if CurrentQuestion != None:
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Getting quiz question answers: " + CurrentQuestion.Name)

        for answer in CurrentQuestion.Answers:
            answerRef = rpmaster_save.main_state().get_quiz_answer_ref(answer) 
            if answerRef != -1:
                blocked = False

                for condition in answerRef.VisibilityConditions:
                    conRef = rpmaster_save.main_state().get_condition_ref(condition)
                    if rpmaster_game_file_condition.evaluate_condition(conRef) == False:
                            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Quiz answer option blocked by condition: " + answerRef.Name  + " / " + conRef.Name)
                            blocked = True
                            return
                    pass
                
                if blocked == False:
                    choices.append(answerRef.AnswerString)
                    answerIdNames.append(answerRef.Name)
                    pass
            pass
        pass

    if len(choices) == 0:
        choices.append('Skip to next question (no valid question answers were found from gamefile))')

    return choices

def execute_question(quizQuestion):
    global CurrentQuestion
    global choices
    global answerIdNames

    if(quizQuestion.is_valid_quiz_question() == False):
        return

    for condition in quizQuestion.VisibilityConditions:
        conRef = rpmaster_save.main_state().get_condition_ref(condition)
        if rpmaster_game_file_condition.evaluate_condition(conRef) == False:
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Quiz question blocked by condition: " + quizQuestion.Name  + " / " + conRef.Name)
                return
        pass

    CurrentQuestion = quizQuestion

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Asking quiz question: " + quizQuestion.Name)

    question = [
        {
            'type': 'rawlist',
            'name': 'menu_option',
            'message': quizQuestion.QuestionString,
            'choices': get_question_choices,
        },
    ]
    
    answers = prompt(question, style=rpm_custom_style)
    command = answers.get('menu_option', '')

    if command == 'Skip to next question':
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "No quiz questions loaded, so skipping: " + quizQuestion.Name)
        return

    index = rpmaster_utilities.index_of(command, choices)

    answerRef = rpmaster_save.main_state().get_quiz_answer_ref(answerIdNames[index]) 

    if answerRef == -1:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find selected answer after quiz " + answerIdNames[index] + " in: " + quizQuestion.Name)
        return

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Executing actions and operations for answer: " + answerIdNames[index])

    for action in answerRef.TriggeredActions:
        actionRef = rpmaster_save.main_state().get_action_ref(action) 
        actionRef.execute_action()
        pass

    for operation in answerRef.TriggeredOperations:
        operationRef = rpmaster_save.main_state().get_operation_ref(operation)
        rpmaster_game_file_operation.execute_operation(operationRef)
        pass

    return