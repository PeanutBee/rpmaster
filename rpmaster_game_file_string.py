import yaml
import io
import os
from os import path
from enum import Enum

import rpmaster_settings
import rpmaster_log
import rpmaster_game_file_variable
import rpmaster_save


defaultName = 'invalid'
defaultAction = 'invalid'


# only standard YAML tag

class RPMasterString(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileString'
    Name = ''
    String = ''

    def __init__(self, Name, String, ):
        self.Name = Name
        self.String = String

    def __repr__(self):
        return "%s(Name=%r, String=%r, )" % (
            self.__class__.__name__, self.Name, self.String, )

    def is_valid_string(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "String has no name")
            return result
            
        return result
