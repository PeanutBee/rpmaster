import yaml
import io
import os
# from yaml import SafeLoader, SafeDumper
from yaml import Loader, Dumper
from os import path
from os import mkdir
from enum import Enum

from clint.textui import colored

import rpmaster_log

class Colors(Enum):
    RED = 'red'
    GREEN = 'green'
    YELLOW = 'yellow'
    BLUE = 'blue'
    BLACK = 'black'
    MAGENTA = 'magenta'
    CYAN = 'cyan'
    WHITE = 'white'

currentRpmVersion = '1.0'
currentSettingsVersion = '1.0'
settingsFileName = 'settings.yaml'

defaultSaveDirectory = "Saves"
defaultGameFileDirectory = "GameFiles"
defaultLogFileDirectory = "Logs"
defaultGenerateCustomGameFile = False
defaultPrintDebug = False
defaultShowDebugFunctions = False

defaultAutoSaveOnExit = True
defaultAutoSaveOnOperation = False
defaultAutoSaveEachDay = True

defaultMaxEquationLength = 200
defaultHistoryMax = 20
defaultAllowUndoBlock = True

defaultRpmErrorColor = Colors.RED
defaultRpmWarningColor = Colors.YELLOW
defaultVariableValueColor = Colors.YELLOW
defaultDateTimeColor = Colors.CYAN
defaultOperationNameColor = Colors.BLUE
defaultActionNameColor = Colors.RED
defaultQuestNameColor = Colors.GREEN
defaultEventNameColor = Colors.RED
defaultPlayerNameColor = Colors.MAGENTA
defaultNpcNameColor = Colors.CYAN

loadedSettings = None


class Settings(yaml.YAMLObject):
    yaml_tag = u'!rpmasterSettings'
    SettingsFileVersion = ''
    SaveDirectory = ''
    GameFileDirectory = ''
    LogFileDirectory = ''

    GenerateCustomGameFile = False

    PrintDebug = False
    ShowDebugFunctions = False

    AutoSaveOnExit = False
    AutoSaveOnOperation = False
    AutoSaveEachDay = False

    MaxVariableHistoryToStore = 0
    AllowGamefileToDisableUndo = True
    MaxEquationLength = 0

    RpmErrorColor = None
    RpmWarningColor = None
    VariableValueColor = None
    DateTimeColor = None
    OperationNameColor = None
    ActionNameColor = None
    QuestNameColor = None
    EventNameColor = None
    PlayerNameColor = None
    NpcNameColor = None

    def __init__(self, SaveDirectory=None):
        self.SettingsFileVersion = currentSettingsVersion
        if(SaveDirectory is None):
            self.SaveDirectory = defaultSaveDirectory
        else:
            self.SaveDirectory = SaveDirectory

        self.GameFileDirectory = defaultGameFileDirectory
        self.LogFileDirectory = defaultLogFileDirectory

        self.GenerateCustomGameFile = defaultGenerateCustomGameFile
        self.PrintDebug = defaultPrintDebug
        self.ShowDebugFunctions = defaultShowDebugFunctions

        self.AutoSaveOnExit = defaultAutoSaveOnExit
        self.AutoSaveOnOperation = defaultAutoSaveOnOperation
        self.AutoSaveEachDay = defaultAutoSaveEachDay

        self.MaxEquationLength = defaultMaxEquationLength
        self.MaxVariableHistoryToStore = defaultHistoryMax
        self.AllowGamefileToDisableUndo = defaultAllowUndoBlock

        self.RpmErrorColor = defaultRpmErrorColor
        self.RpmWarningColor = defaultRpmWarningColor
        self.VariableValueColor = defaultVariableValueColor
        self.DateTimeColor = defaultDateTimeColor
        self.OperationNameColor = defaultOperationNameColor
        self.ActionNameColor = defaultActionNameColor
        self.QuestNameColor = defaultQuestNameColor
        self.EventNameColor = defaultEventNameColor
        self.PlayerNameColor = defaultPlayerNameColor
        self.NpcNameColor = defaultNpcNameColor

    def __repr__(self):
        return "%s(SettingsFileVersion=%r, SaveDirectory=%r, GameFileDirectory=%r, LogFileDirectory=%r, GenerateCustomGameFile=%r, PrintDebug=%r, ShowDebugFunctions=%r, AutoSaveOnExit=%r, AutoSaveOnOperation=%r, AutoSaveEachDay=%r, MaxEquationLength=%r, MaxVariableHistoryToStore=%r, AllowGamefileToDisableUndo=%r, RpmErrorColor=%r, RpmWarningColor=%r, VariableValueColor=%r, DateTimeColor=%r, OperationNameColor=%r, ActionNameColor=%r, QuestNameColor=%r, EventNameColor=%r, PlayerNameColor=%r, NpcNameColor=%r, )" % (
            self.__class__.__name__, self.SettingsFileVersion, self.SaveDirectory, self.GameFileDirectory, self.LogFileDirectory, self.GenerateCustomGameFile, self.PrintDebug, self.ShowDebugFunctions, self.AutoSaveOnExit, self.AutoSaveOnOperation, self.AutoSaveEachDay, self.MaxEquationLength, self.MaxVariableHistoryToStore, self.AllowGamefileToDisableUndo, self.RpmErrorColor, self.RpmWarningColor, self.VariableValueColor, self.DateTimeColor, self.OperationNameColor, self.ActionNameColor, self.QuestNameColor, self.EventNameColor, self.PlayerNameColor, self.NpcNameColor, )


    def get_save_directory(self):
        # To protect against users not ending the path with a backslash
        if(self.SaveDirectory == ''):
            return self.SaveDirectory
        else:
            return self.SaveDirectory + "\\"


    def get_game_file_directory(self):
        # To protect against users not ending the path with a backslash
        if(self.GameFileDirectory == ''):
            return self.GameFileDirectory
        else:
            return self.GameFileDirectory + "\\"


    def get_log_file_directory(self):
        # To protect against users not ending the path with a backslash
        if(self.LogFileDirectory == ''):
            return self.LogFileDirectory
        else:
            return self.LogFileDirectory + "\\"

    def get_max_history_size(self):
        return self.MaxVariableHistoryToStore



####################################################################################################

def initialise():
    global loadedSettings
    

    loadedSettings = Settings()
    if(path.exists(settingsFileName) == False):
        print("No settings file found, creating new one")
        with open(settingsFileName, 'w+') as settingsFile:
            # yaml.safe_dump(loadedSettings, settingsFile)
            # yaml.dump(loadedSettings, settingsFile, canonical=True)
            yaml.Dumper.ignore_aliases = lambda *args : True
            yaml.dump(loadedSettings, settingsFile)
    else:
        with open(settingsFileName, 'r') as settingsFile:
            # loadedSettings = yaml.safe_load(settingsFile)
            loadedSettings = yaml.load(settingsFile, Loader=Loader)

    if(loadedSettings.SettingsFileVersion != currentSettingsVersion):
        print("WARNING: Loaded settings file was written by an old version and may cause errors")
        
    print("Loaded RPM settings")

    
    savePath = loadedSettings.get_save_directory()
    gameFilePath = loadedSettings.get_game_file_directory()
    logPath = loadedSettings.get_log_file_directory()

    currentDirectory = os.getcwd()

    if(path.isabs(savePath) == True):
        if(path.exists(savePath) == False):
            mkdir(savePath)
    else:
        if(path.exists(currentDirectory + '\\' + savePath) == False):
            mkdir(currentDirectory  + '\\' +  savePath)

    if(path.isabs(gameFilePath) == True):
        if(path.exists(gameFilePath) == False):
            mkdir(gameFilePath)
    else:
        if(path.exists(currentDirectory + '\\' + gameFilePath) == False):
            mkdir(currentDirectory  + '\\' +  gameFilePath)

    if(path.isabs(logPath) == True):
        if(path.exists(logPath) == False):
            mkdir(logPath)
    else:
        if(path.exists(currentDirectory + '\\' + logPath) == False):
            mkdir(currentDirectory + '\\' + logPath)

    return


def is_settings_file_path_valid(settingsFileName):
    if(path.exists(settingsFileName) == False):
        return False
    else:
        return True


def loaded_settings():
    global loadedSettings
    return loadedSettings


def get_color(color):
    result = None

    if(color == Colors.RED):
        result = getattr(colored, 'red')
        pass
    if(color == Colors.GREEN):
        result = getattr(colored, 'green')
        pass
    if(color == Colors.YELLOW):
        result = getattr(colored, 'yellow')
        pass
    if(color == Colors.BLUE):
        result = getattr(colored, 'blue')
        pass
    if(color == Colors.BLACK):
        result = getattr(colored, 'black')
        pass
    if(color == Colors.MAGENTA):
        result = getattr(colored, 'magenta')
        pass
    if(color == Colors.CYAN):
        result = getattr(colored, 'cyan')
        pass
    if(color == Colors.WHITE):
        result = getattr(colored, 'white')
        pass

    return result

def error_color():
    return get_color(loaded_settings().RpmErrorColor)
def warning_color():
    return get_color(loaded_settings().RpmWarningColor)
def variable_color():
    return get_color(loaded_settings().VariableValueColor)
def time_color():
    return get_color(loaded_settings().DateTimeColor)
def operation_color():
    return get_color(loaded_settings().OperationNameColor)
def action_color():
    return get_color(loaded_settings().ActionNameColor)
def quest_color():
    return get_color(loaded_settings().QuestNameColor)
def event_color():
    return get_color(loaded_settings().EventNameColor)
def player_color():
    return get_color(loaded_settings().PlayerNameColor)
def npc_color():
    return get_color(loaded_settings().NpcNameColor)