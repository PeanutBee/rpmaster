from __future__ import print_function, unicode_literals
import regex
import sys
import os
import psutil
import logging
from pprint import pprint

from PyInquirer import style_from_dict, Token, prompt, print_json, Separator, color_print
from PyInquirer import Validator, ValidationError


from clint.eng import join
from clint.textui import colored, indent, puts
from clint.textui import columns


import rpmaster_save
import rpmaster_log
import rpmaster_game_file_variable
from rpmaster_utilities import rpm_custom_style



def menu_history():
    menu_status_choices = [
        {
            'type': 'list',
            'name': 'menu_option',
            'message': 'What would you like to do?',
            'choices': [
                'Return',
            ]
        },
    ]

    print("Recent game log:")

    print("")

    print_history()

    print("")

    answers = prompt(menu_status_choices, style=rpm_custom_style)
    command = answers.get('menu_option', '')

    if(command == 'Return'):
        return

    return


def print_history():

    history = rpmaster_log.get_game_log_history()

    for message in history:
        with indent(4, quote=colored.blue('>')):
            puts(message)
        pass


    return
