from __future__ import print_function, unicode_literals
import regex
import sys
import os
import psutil
import enum
import logging
from pprint import pprint

from PyInquirer import style_from_dict, Token, prompt, print_json, Separator, color_print
from PyInquirer import Validator, ValidationError

from rpmaster_utilities import rpm_custom_style

from clint.eng import join
from clint.textui import colored, indent, puts
from clint.textui import columns

import rpmaster_settings
import rpmaster_save
import rpmaster_log
import rpmaster_game_file_variable
import rpmaster_game_file_event



def menu_event():

    nextEventRef = rpmaster_game_file_event.get_next_event()

    execute_event(nextEventRef)
    
    return


def execute_event(event):

    if(rpmaster_settings.loaded_settings().AutoSaveOnOperation == True):
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "On event auto save")
        rpmaster_save.save_state()

    rpmaster_save.main_state().store_variables_to_history(event.Name)

    rpmaster_game_file_event.execute_event(event)

    green = getattr(colored, 'green')
    input(green("Operation complete...")) # Causes the game log to hold until user progresses

    return
