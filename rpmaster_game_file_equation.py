import yaml
import io
import os

import random
from os import path
from enum import Enum

from asteval import Interpreter

import rpmaster_log
import rpmaster_settings
import rpmaster_save
import rpmaster_game_file_variable

def is_safeish_equation(equation):
    result = True

    vType = type(equation)

    if(vType is not str):
        result = False
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Equation variable type is not string")

    if len(equation) > rpmaster_settings.loaded_settings().MaxEquationLength:
        result = False
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Equation exceeds maximum length (" + str(rpmaster_settings.loaded_settings().MaxEquationLength) + "): " + equation)

    return result


def evaluate(variables, equation):
    result = 0

    if is_safeish_equation(equation) == True:
        aeval = Interpreter()

        for var in variables:

            varRef = rpmaster_save.main_state().get_variable_ref(var, True, False)
            varValue = varRef.get_variable_value()
            aeval.symtable[varRef.Name] = varValue
            pass

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating equation: " + equation)

        result = aeval(equation)

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Equation result: " + str(result))
        pass
    else:
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Given equation does not fit within restrictions: " + '"' + equation + '"')
        pass

    return result
