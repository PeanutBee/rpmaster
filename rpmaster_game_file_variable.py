import yaml
import io
import os
from os import path
from enum import Enum

import random
import datetime

import rpmaster_settings
import rpmaster_log
import rpmaster_save
import rpmaster_game_file_condition
import rpmaster_game_file_event


class VariableTypes(Enum):
    INVALID = 'invalid'
    HARDCODED = 'hardcoded'
    INT = 'int'
    FLOAT = 'float'
    BOOL = 'bool'
    PERCENTAGE = 'percentage'  # Not implemented yet
    DATETIME = 'datetime'
    ENUM = 'enum'  # Not implemented yet
    OPERATION_REFERENCE = 'operation_reference'  # Not implemented yet
    EVENT_REFERENCE = 'event_reference'  # Not implemented yet
    ACTION_REFERENCE = 'action_reference'  # Not implemented yet
    QUEST_REFERENCE = 'quest_reference'  # Not implemented yet
    STRING_REFERENCE = 'string_reference'  # Not implemented yet
    CHARACTER_REFERENCE = 'character_reference'  # Not implemented yet
    EVENT_QUEUE = 'event_queue'

defaultVariableName = 'invalid'
defaultPrettyName = 'New variable'
defaultVariableComments = 'None'
defaultType = VariableTypes.INVALID


# only standard YAML tag

class Variable(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileVariable'
    Name = ''
    PrettyName = ''
    Comments = ''
    Type = None
    Min = None
    Max = None
    Value = None

    AlwaysHideFromPlayer = False
    VisibilityConditions = []

    def __init__(self, Name):
        self.Name = Name

        self.PrettyName = defaultPrettyName
        self.Comments = defaultVariableComments
        self.Type = defaultType
        self.Min = None
        self.Max = None
        self.Value = None

        self.AlwaysHideFromPlayer = False
        self.VisibilityConditions = []

    def __repr__(self):
        return "%s(Name=%r, PrettyName=%r, Comments=%r, Type=%r, Min=%r, Max=%r, Value=%r, AlwaysHideFromPlayer=%r, VisibilityConditions=%r, )" % (
            self.__class__.__name__, self.Name, self.PrettyName, self.Comments, self.Type, self.Min, self.Max, self.Value, self.AlwaysHideFromPlayer, self.VisibilityConditions, )

    def is_valid_variable(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable has no name")
            return result

        if len(self.PrettyName) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable has no pretty name")
            return result

        if (' ' in self.Name): 
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variables must not have spaces in their name so that equations can reference them: " + self.Name)
            return False

        if(self.Type == VariableTypes.INVALID):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Invalid variable type found: " + self.Name)
            return False

        # if(self.Name == "HardcodedFloat0_1"):
        #     rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable name conflicts with hardcoded variable: " + self.Name)
        #     return False
        # if(self.Name == "HardcodedFloat0_100"):
        #     rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable name conflicts with hardcoded variable: " + self.Name)
        #     return False
        # if(self.Name == "HardcodedInt0_100"):
        #     rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable name conflicts with hardcoded variable: " + self.Name)
        #     return False

        vType = type(self.Value)

        if(self.Type == VariableTypes.INT):
            if(vType is not int):
                result = False
            
            if (self.Max != None) and (self.Min != None):
                if self.Min > self.Max:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable minimum is higher than maximum: " + self.Name)
                    return result
            if (self.Min != None):
                if self.Value < self.Min:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable is lower than minimum: " + self.Name)
            if (self.Max != None):
                if self.Value > self.Max:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable is higher than maximum: " + self.Name)
                
        elif(self.Type == VariableTypes.FLOAT):
            if((vType is not float) and (vType is not int)):
                result = False
            if (self.Max != None) and (self.Min != None):
                if self.Min > self.Max:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable minimum is higher than maximum: " + self.Name)
                    return result
            if (self.Min != None):
                if self.Value < self.Min:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable is lower than minimum: " + self.Name)
            if (self.Max != None):
                if self.Value > self.Max:
                    result = False
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Variable is higher than maximum: " + self.Name)
                
        elif(self.Type == VariableTypes.BOOL):
            if(vType is not bool):
                result = False

        elif(self.Type == VariableTypes.DATETIME):
            if(isinstance(self.Value, datetime.datetime) == False):
                result = False

        elif(self.Type == VariableTypes.EVENT_QUEUE):
            if(isinstance(self.Value, rpmaster_game_file_event.EventQueue) == False):
                result = False

        if(result == False):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Corrupt variable found: " + self.Name)

        return result

    def is_visible(self):
        result = True
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Testing visibility for variable: " + self.Name)

        if rpmaster_settings.loaded_settings().ShowDebugFunctions == False: # Always show everything if we're in debug mode
            if(self.AlwaysHideFromPlayer == True):# Means that the always hide takes priority over the conditions
                result = False
                
            for condition in self.VisibilityConditions:
                conRef = rpmaster_save.main_state().get_condition_ref(condition)
                if rpmaster_game_file_condition.evaluate_condition(conRef) == False:
                    result = False
                    pass
                pass

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Tested visibility for variable: " + self.Name + ' - ' + str(result))

        return result

    def get_variable_value(self):

        if(self.is_valid_variable() == False):
            return -1

        if(self.Name == "HardcodedFloat0_1"):
            return random.random()
        elif(self.Name == "HardcodedFloat0_100"):
            return random.uniform(0, 100)
        elif(self.Name == "HardcodedInt0_100"):
            return random.randint(0, 100)

        return self.Value

    def set_variable_value(self, newValue):
        result = False

        if(self.is_valid_variable() == False):
            return result

        if(self.Name == "HardcodedFloat0_1"):
            return result
        elif(self.Name == "HardcodedFloat0_100"):
            return result
        elif(self.Name == "HardcodedInt0_100"):
            return result

        vType = type(newValue)

        if(self.Type == VariableTypes.INT):
            if(vType is int or vType is float ):
                result = True
        elif(self.Type == VariableTypes.FLOAT):
            if(vType is float or vType is int):
                result = True
        elif(self.Type == VariableTypes.BOOL):
            if(vType is bool):
                result = True
        elif(self.Type == VariableTypes.DATETIME):
            if(isinstance(newValue, datetime.datetime) == True):
                result = True

        if(result == True):
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Changing variable, old value: " + self.Name + ' ' + str(self.Value))

            if(self.Type == VariableTypes.INT):
                self.Value = int(self.check_min_max(newValue))
            elif(self.Type == VariableTypes.FLOAT):
                self.Value = float(self.check_min_max(newValue))
            elif(self.Type == VariableTypes.BOOL):
                self.Value = bool(newValue)
            elif(self.Type == VariableTypes.DATETIME):
                self.Value = newValue

            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Changing variable, new value: " + self.Name + ' ' + str(self.Value))
        else:
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "New variable value does not match variable type: " + self.Name + ' - ' + str(newValue))

        return result

    def check_min_max(self, newValue):
        if(self.Type == VariableTypes.INT or self.Type == VariableTypes.FLOAT):
            if (self.Max != None) and (newValue > self.Max):
                newValue = self.Max
            if (self.Min != None) and (newValue < self.Min):
                newValue = self.Min

        return newValue
