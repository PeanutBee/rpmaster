import yaml
import io
import os
from os import path
from enum import Enum

import rpmaster_settings
import rpmaster_log
import rpmaster_game_file_equation
import rpmaster_save


# only standard YAML tag

class RootMenuVariable(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileRootMenuVariable'
    VariableReference = ''
    Equation = ''
    VariablesUsed = [] # array of name references
    EvaluationThresholds = []  # thresholds hold the action to take reference

    def __init__(self, VariableReference,):
        self.VariableReference = VariableReference
        self.Equation = ''
        self.VariablesUsed = []
        self.EvaluationThresholds = []

    def __repr__(self):
        return "%s(VariableReference=%r, Equation=%r, VariablesUsed=%r, EvaluationThresholds=%r, )" % (
            self.__class__.__name__, self.VariableReference, self.Equation, self.VariablesUsed, self.EvaluationThresholds, )

    def is_valid_menu_variable(self):
        result = True

        if len(self.VariableReference) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Root menu variable has no variable reference")
            return result

        if(rpmaster_game_file_equation.is_safeish_equation(self.Equation) == False):
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Invalid equation found in root menu variable: " + self.VariableReference)
            
        for variable in self.VariablesUsed:
            if rpmaster_save.main_state().get_variable_ref(variable, True, False, printLog = False) == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find used variable " + variable + " for root menu variable: " + self.VariableReference)
            pass

        for threshold in self.EvaluationThresholds:
            if threshold.is_valid_threshold() == -1:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Could not find used variable " + threshold.Name + " for root menu variable: " + self.VariableReference)
            pass

        return result
        
    def check_threshold(self, value):
        color = rpmaster_settings.get_color(rpmaster_settings.Colors.WHITE)
        foundResult = False

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating " + str(len(self.EvaluationThresholds)) + " thresholds against value " + str(value))

        for threshold in self.EvaluationThresholds:
            if threshold.UpperBound != None:
                if value < threshold.UpperBound:
                    if threshold.LowerBound == None:
                        color = rpmaster_settings.get_color(threshold.Color)
                        foundResult = True
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
                        break
                    elif value >= threshold.LowerBound:
                        color = rpmaster_settings.get_color(threshold.Color)
                        foundResult = True
                        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
                        break
                    # else:
                    #     rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold test value below hard lower limit")
                    #     foundResult = True
                    #     break
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Passed threshold: " + threshold.Name)

        if foundResult == False:
            if threshold.UpperBound == None:
                color = rpmaster_settings.get_color(threshold.Color)
                rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Matched threshold: " + threshold.Name)
            else:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold test did not find a valid match: " + str(value))

        return color

def get_variable_color(menuVariable):
    color = rpmaster_settings.get_color(rpmaster_settings.Colors.WHITE)

    if(menuVariable.is_valid_menu_variable() == False):
        return color

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Evaluating root variable color: " + menuVariable.VariableReference)

    equationResult = rpmaster_game_file_equation.evaluate(menuVariable.VariablesUsed, menuVariable.Equation)

    color = menuVariable.check_threshold(equationResult)

    return color