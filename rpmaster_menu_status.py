from __future__ import print_function, unicode_literals
import regex
import sys
import os
import psutil
import enum
import logging
from pprint import pprint

from PyInquirer import style_from_dict, Token, prompt, print_json, Separator, color_print
from PyInquirer import Validator, ValidationError

from rpmaster_utilities import rpm_custom_style

from clint.eng import join
from clint.textui import colored, indent, puts
from clint.textui import columns

import rpmaster_save
import rpmaster_log
import rpmaster_game_file_variable



nonDisplayableVariableTypes = [rpmaster_game_file_variable.VariableTypes.EVENT_QUEUE, ]


def menu_status():
    menu_status_choices = [
        {
            'type': 'list',
            'name': 'menu_option',
            'message': 'What would you like to do?',
            'choices': [
                'Return',
            ]
        },
    ]

    print_status()

    answers = prompt(menu_status_choices, style=rpm_custom_style)
    command = answers.get('menu_option', '')

    if(command == 'Return'):
        return

    return


def print_status():
    global nonDisplayableVariableTypes

    variableList = rpmaster_save.main_state().get_variable_list()

    MajorColumnWidth = 60
    SeperatorColumnWidth = 1

    horizontalMarker = '-'
    verticalMarker = '|'
    horizontalLine = horizontalMarker * MajorColumnWidth

    colors = [
        'blue',
        'green',
    ]
    colors = [str(cs) for cs in colors]

    puts(columns(
        [(colored.yellow('Name')), MajorColumnWidth],
        [verticalMarker, SeperatorColumnWidth],
        [(colored.yellow('Value')), MajorColumnWidth],
        [verticalMarker, SeperatorColumnWidth]))
        # [(colored.yellow('???')), MajorColumnWidth]))

    puts(columns(
        [horizontalLine, MajorColumnWidth],
        [verticalMarker, SeperatorColumnWidth],
        [horizontalLine, MajorColumnWidth],
        [verticalMarker, SeperatorColumnWidth]))
        # [horizontalLine, MajorColumnWidth]))

    lineNo = 0
    for variable in variableList:
        varRef = rpmaster_save.main_state().get_variable_ref(variable, True, False, printLog = False)
        name = varRef.PrettyName

        visible = varRef.is_visible()

        for nonDisplayableType in nonDisplayableVariableTypes:
            if varRef.Type == nonDisplayableType:
                visible = False
                pass
            pass

        if(visible == True):

            if varRef.Type == rpmaster_game_file_variable.VariableTypes.FLOAT:
                value = format(varRef.get_variable_value(), '.2f')
                pass
            else:
                value = str(varRef.get_variable_value())
                pass

            colorIndex = lineNo % (len(colors))
            color = colors[colorIndex]

            puts(columns(
                [getattr(colored, color)(name), MajorColumnWidth],
                [verticalMarker, SeperatorColumnWidth],
                [getattr(colored, color)(value), MajorColumnWidth],
                [verticalMarker, SeperatorColumnWidth]))
                # [getattr(colored, color)('???'), MajorColumnWidth]))

            lineNo += 1
        pass

    puts(horizontalLine + '---' + horizontalLine + '--' )

    return
