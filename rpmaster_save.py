import yaml
import io
import os
# from yaml import SafeLoader, SafeDumper
from yaml import Loader, Dumper
from os import path
import copy

import rpmaster_settings
import rpmaster_log
import rpmaster_time
import rpmaster_game_file
import rpmaster_game_file_event
import rpmaster_game_file_variable
import rpmaster_game_file_collection

from rpmaster_utilities import index_of

currentSaveFileVersion = '0.0'
defaultStateName = 'New Save'

####################################
mainRPMasterState = None
####################################
randomFloatVariable0_1 = None
randomFloatVariable0_100 = None
randomIntVariable0_100 = None
####################################

####################################################################################################

class VariablesStore(yaml.YAMLObject):
    yaml_tag = u'!rpmasterStateVariablesBackup'
    Variables = []
    StoredBeforeOperation = ''
    StoredDatetime = None

    def __init__(self, Variables, StoredBeforeOperation):
        self.Variables = copy.deepcopy(Variables)
        self.StoredBeforeOperation = StoredBeforeOperation

        if rpmaster_time.is_time_enabled() == True:
            self.StoredDatetime = rpmaster_time.get_current_datetime()
            pass

    def __repr__(self):
        return "%s(Variables=%r, BackedupBeforeOperation=%r, )" % (
            self.__class__.__name__, self.Variables, self.StoredBeforeOperation, )


####################################################################################################

class rpmasterState(yaml.YAMLObject):
    yaml_tag = u'!rpmasterState'
    SaveFileVersion = ''
    Name = ''
    StateSaveNumber = 0
    LoadedCorrectly = False
    InitialisationComplete = False
    GameFile = None
    LiveVariables = []
    VariableHistory = []

    def __init__(self, StateName=None):
        self.SaveFileVersion = currentSaveFileVersion
        if(StateName is None):
            self.Name = defaultStateName
        else:
            self.Name = StateName

        self.StateSaveNumber = 0
        self.LoadedCorrectly = False
        self.InitialisationComplete = False

        self.GameFile = rpmaster_game_file.get_loaded_game_file()

        self.LiveVariables = []
        self.VariableHistory = []

    def __repr__(self):
        return "%s(SaveFileVersion=%r, Name=%r, StateSaveNumber=%r, LoadedCorrectly=%r, InitialisationComplete=%r, GameFile=%r, LiveVariables=%r, VariableHistory=%r, )" % (
            self.__class__.__name__, self.SaveFileVersion, self.Name, self.StateSaveNumber, self.LoadedCorrectly, self.InitialisationComplete, self.GameFile, self.LiveVariables, self.VariableHistory, )

    def write_save(self, savePath):
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Writing save file: " + self.Name)
        self.LoadedCorrectly = False
        self.StateSaveNumber += 1
        with open(savePath, 'w+') as saveFile:
            # yaml.safe_dump(mainRPMasterState, saveFile)
            # yaml.dump(mainRPMasterState, saveFile, canonical=True)
            yaml.Dumper.ignore_aliases = lambda *args : True
            yaml.dump(self, saveFile)
        self.LoadedCorrectly = True
        return

        
    def is_valid_save(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Save has no name")

        if self.StateSaveNumber < 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "State somehow has a negative save number...: " + self.Name)

        if self.LoadedCorrectly == False:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Save has not loaded correctly: " + self.Name)

        if self.GameFile.LoadedCorrectly == False:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Save game file is not loaded correctly: " + self.Name)

        for variable in self.LiveVariables:
            if variable.is_valid_variable() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken collection in: " + self.Name + " - " + variable.Name)
            pass

        for operation in self.GameFile.Operations:
            if operation.is_valid_operation() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken operation in: " + self.Name + " - " + operation.Name)
            pass

        for action in self.GameFile.Actions:
            if action.is_valid_action() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken action in: " + self.Name + " - " + action.Name)
            pass

        for equation in self.GameFile.ActionEquations:
            if equation.is_valid_equation() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken action equation in: " + self.Name + " - " + equation.Name)
            pass

        for string in self.GameFile.Strings:
            if string.is_valid_string() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken string in: " + self.Name + " - " + string.Name)
            pass

        for collection in self.GameFile.Collections:
            if collection.is_valid_collection() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken collection in: " + self.Name + " - " + collection.Name)
            pass


        for menuVariable in self.GameFile.RootMenuVariables:
            if menuVariable.is_valid_menu_variable() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken menu variable in: " + self.Name + " - " + menuVariable.VariableReference)
            pass

        for menuStatus in self.GameFile.RootMenuStatusMessages:
            if menuStatus.is_valid_status() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken menu status in: " + self.Name + " - " + menuStatus.Name)
            pass

        for event in self.GameFile.Events:
            if event.is_valid_event() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken string in: " + self.Name + " - " + event.Name)
            pass

        for quizGroup in self.GameFile.QuizGroups:
            if quizGroup.is_valid_quiz_group() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken quiz group in: " + self.Name + " - " + quizGroup.Name)
            pass
        for quizQuestion in self.GameFile.QuizQuestions:
            if quizQuestion.is_valid_quiz_question() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken quiz question in: " + self.Name + " - " + quizQuestion.Name)
            pass
        for quizAnswer in self.GameFile.QuizAnswers:
            if quizAnswer.is_valid_quiz_answer() == False:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Found broken quiz answer in: " + self.Name + " - " + quizAnswer.Name)
            pass

        return result

####################################################################################################

    def invalid_check(self, name):
        result = True

        if (name == 'invalid'):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Invalid object name: " + name)
            result = False
        if (name == None):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Object name not set")
            result = False
        if (name == -1):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Object name not set")
            result = False
            
        return result

    def store_variables_to_history(self, operationName):
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, 'Storring current variables state to history before operation: ' + operationName)
        
        maxLength = rpmaster_settings.loaded_settings().get_max_history_size()

        while len(self.VariableHistory) > maxLength:
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "History is at limit ("+ str(maxLength) +"), deleting oldest")
            self.VariableHistory.pop(0)

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "History below limit: " + str(len(self.VariableHistory)) + '/' + str(maxLength))
        store = VariablesStore(copy.deepcopy(self.LiveVariables), operationName)
        self.VariableHistory.append(store)
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "New history length: " + str(len(self.VariableHistory)) + '/' + str(maxLength))
        return

    def load_variables_from_history(self, index):
        if(index < 0):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Requested variable history index is less than 0")
            return None
        if(index >= len(self.VariableHistory)):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Requested variable history index is higher than stored length: " + index + '/' + len(self.VariableHistory))
            return None

        store = self.get_var_history_ref(index)

        rpmaster_log.debug_log(rpmaster_log.Level.INFO, 'Restoring variables state from history before operation: ' + store.StoredBeforeOperation)

        self.LiveVariables = copy.deepcopy(store.Variables)

        while index <= len(self.VariableHistory) - 1:
            removeTarget = len(self.VariableHistory) - 1
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, 'Removing stored history newer than rollback point: ' + self.VariableHistory[removeTarget].StoredBeforeOperation)
            self.VariableHistory.pop(removeTarget)
        return
        
    def delete_variables_history(self, operationName):
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, 'Deleting variables history at request of operation/event: ' + operationName)
        
        self.VariableHistory.clear()
        return

    def get_var_history_list(self):
        backups = []

        for backup in self.VariableHistory:
            backups.append([backup.StoredBeforeOperation, backup.StoredDatetime])

        return backups

    def get_var_history_ref(self, index):
        if(index < 0):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Requested variable history index is less than 0")
            return None
        if(index >= len(self.VariableHistory)):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Requested variable history index is higher than stored length: " + index + '/' + len(self.VariableHistory))
            return None

        return self.VariableHistory[index]
        

    def get_root_menu_variable_list(self):
        menuVariables = []

        for menuVar in self.GameFile.RootMenuVariables:
            menuVariables.append(menuVar.VariableReference)

        return menuVariables

    def get_root_menu_variable_ref(self, name, reportError = True):
        if self.invalid_check(name) == False:
            return -1

        variables = self.get_root_menu_variable_list()

        index = index_of(name, variables)

        if(index == -1):
            if reportError == True:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Menu variable "' + name + '" not found in menu variable list')
            return -1

        return self.GameFile.RootMenuVariables[index]

    def get_root_menu_status_list(self):
        menuStatuses = []

        for menuStatus in self.GameFile.RootMenuStatusMessages:
            menuStatuses.append(menuStatus.Name)

        return menuStatuses

    def get_root_menu_status_ref(self, name, reportError = True):
        if self.invalid_check(name) == False:
            return -1

        statuses = self.get_root_menu_status_list()

        index = index_of(name, statuses)

        if(index == -1):
            if reportError == True:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Menu status "' + name + '" not found in manu status list')
            return -1

        return self.GameFile.RootMenuStatusMessages[index]

    def get_variable_list(self):
        variables = []

        for variable in self.LiveVariables:
            variables.append(variable.Name)

        return variables

    def get_variable_pretty_list(self):
        variables = []

        for variable in self.LiveVariables:
            variables.append(variable.PrettyName)

        return variables

    def get_variable_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        global randomFloatVariable0_1
        global randomFloatVariable0_100
        global randomIntVariable0_100

        if(name == "HardcodedFloat0_1"):
            return randomFloatVariable0_1
        if(name == "HardcodedFloat0_100"):
            return randomFloatVariable0_100
        if(name == "HardcodedInt0_100"):
            return randomIntVariable0_100

        variables = self.get_variable_list()

        index = index_of(name, variables)

        if(index == -1):
            if checkCollections == True:
                varName = self.find_in_collections(name, reportError, printLog)

                if(varName == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Variable "' + name + '" not found in either variable list or collection list')
                    return -1

                index = index_of(varName, variables)
                if(index == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Variable "' + varName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Variable "' + name + '" not found in variable list')
                return -1

        return self.LiveVariables[index]


    def get_operation_list(self):
        operations = []

        for operation in self.GameFile.Operations:
            operations.append(operation.Name)

        return operations

    def get_operation_pretty_list(self):
        operations = []

        for operation in self.GameFile.Operations:
            operations.append(operation.PrettyName)

        return operations


    def get_operation_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        operations = self.get_operation_list()

        index = index_of(name, operations)

        if(index == -1):
            if checkCollections == True:
                opName = self.find_in_collections(name, reportError, printLog)

                if(opName == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Operation "' + name + '" not found in either operation list or collection list')
                    return -1

                index = index_of(opName, operations)
                if(index == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Operation "' + opName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Operation "' + name + '" not found in operation list')
                return -1

        return self.GameFile.Operations[index]

    def get_action_list(self):
        actions = []

        for action in self.GameFile.Actions:
            actions.append(action.Name)

        return actions


    def get_action_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        actions = self.get_action_list()

        index = index_of(name, actions)

        if(index == -1):
            if checkCollections == True:
                actName = self.find_in_collections(name, reportError, printLog)

                if(actName == -1 or actName == None):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Action "' + name + '" not found in either action list or collection list')
                    return -1

                index = index_of(actName, actions)
                if(index == -1):
                    if reportError == True:
                        print(actName)
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Action "' + actName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Action "' + name + '" not found in action list')
                return -1

        return self.GameFile.Actions[index]

    def get_action_equation_list(self):
        equations = []

        for equation in self.GameFile.ActionEquations:
            equations.append(equation.Name)

        return equations


    def get_action_equation_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        equations = self.get_action_equation_list()

        index = index_of(name, equations)

        if(index == -1):
            if checkCollections == True:
                eqName = self.find_in_collections(name, reportError, printLog)

                if(eqName == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Action equation "' + name + '" not found in either action equation list or collection list')
                    return -1

                index = index_of(eqName, equations)
                if(index == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Action equation "' + eqName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Action equation "' + name + '" not found in action equation list')
                return -1

        return self.GameFile.ActionEquations[index]

    def get_string_list(self):
        strings = []

        for string in self.GameFile.Strings:
            strings.append(string.Name)

        return strings


    def get_string_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        strings = self.get_string_list()

        index = index_of(name, strings)

        if(index == -1):
            if checkCollections == True:
                strName = self.find_in_collections(name, reportError, printLog)

                if(strName == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'String "' + name + '" not found in either string list or collection list')
                    return -1

                index = index_of(strName, strings)
                if(index == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'String "' + strName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'String "' + name + '" not found in String list')
                return -1

        return self.GameFile.Strings[index]


    def get_event_list(self):
        events = []

        for event in self.GameFile.Events:
            events.append(event.Name)

        return events


    def get_event_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        events = self.get_event_list()

        index = index_of(name, events)

        if(index == -1):
            if checkCollections == True:
                evtName = self.find_in_collections(name, reportError, printLog)

                if(evtName == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Event "' + name + '" not found in either event list or collection list')
                    return -1

                index = index_of(evtName, events)
                if(index == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Event "' + evtName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Event "' + name + '" not found in event list')
                return -1

        return self.GameFile.Events[index]


    def get_quiz_group_list(self):
        quizGroups = []

        for quizGroup in self.GameFile.QuizGroups:
            quizGroups.append(quizGroup.Name)

        return quizGroups


    def get_quiz_group_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        quizGroups = self.get_quiz_group_list()

        index = index_of(name, quizGroups)

        if(index == -1):
            if checkCollections == True:
                qGrpName = self.find_in_collections(name, reportError, printLog)

                if(qGrpName == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz group "' + name + '" not found in either quiz group list or collection list')
                    return -1

                index = index_of(qGrpName, quizGroups)
                if(index == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz group "' + qGrpName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz group "' + name + '" not found in quiz group list')
                return -1

        return self.GameFile.QuizGroups[index]

    def get_quiz_question_list(self):
        quizQuestions = []

        for quizQuestion in self.GameFile.QuizQuestions:
            quizQuestions.append(quizQuestion.Name)

        return quizQuestions


    def get_quiz_question_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        quizQuestions = self.get_quiz_question_list()

        index = index_of(name, quizQuestions)

        if(index == -1):
            if checkCollections == True:
                evtName = self.find_in_collections(name, reportError, printLog)

                if(evtName == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz question "' + name + '" not found in either quiz question list or collection list')
                    return -1

                index = index_of(evtName, quizQuestions)
                if(index == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz question "' + evtName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz question "' + name + '" not found in quiz question list')
                return -1

        return self.GameFile.QuizQuestions[index]

    def get_quiz_answer_list(self):
        quizAnswers = []

        for quizAnswer in self.GameFile.QuizAnswers:
            quizAnswers.append(quizAnswer.Name)

        return quizAnswers


    def get_quiz_answer_ref(self, name, reportError = True, checkCollections = True, printLog = True):
        if self.invalid_check(name) == False:
            return -1

        quizAnswers = self.get_quiz_answer_list()

        index = index_of(name, quizAnswers)

        if(index == -1):
            if checkCollections == True:
                evtName = self.find_in_collections(name, reportError, printLog)

                if(evtName == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz answer "' + name + '" not found in either quiz answer list or collection list')
                    return -1

                index = index_of(evtName, quizAnswers)
                if(index == -1):
                    if reportError == True:
                        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz answer "' + evtName + '" not found in collection list "' + name + '"')
                    return -1
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, 'Quiz answer "' + name + '" not found in quiz answer list')
                return -1

        return self.GameFile.QuizAnswers[index]


    def get_collection_list(self):
        collections = []

        for collection in self.GameFile.Collections:
            collections.append(collection.Name)

        return collections


    def get_collection_ref(self, name, reportError = True):
        if self.invalid_check(name) == False:
            return -1

        collections = self.get_collection_list()

        index = index_of(name, collections)

        if(index == -1):
            if reportError == True:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Value not found in collections list: " + name)
            return -1

        return self.GameFile.Collections[index]

    def find_in_collections(self, name, reportError = True, printLog = True):
        collections = self.get_collection_list()
        index = index_of(name, collections)
        
        if(index == -1):
            if reportError == True:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Value not found in collections list: " + name)
            return -1
        else:
            colRef = self.GameFile.Collections[index]
            result = colRef.get_value(False, printLog)

        if(result == -1):
            if reportError == True:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Invalid result returned from collections list: " + name)
            return -1

        return result

    def get_condition_group_list(self):
        conditionGroups = []

        for group in self.GameFile.ConditionGroups:
            conditionGroups.append(group.Name)

        return conditionGroups

    # For whatever reason, it feels like conditions should not support being held in collections, but this may be changed in future
    def get_condition_group_ref(self, name, reportError = True):
        if self.invalid_check(name) == False:
            return -1

        conditionGroups = self.get_condition_group_list()

        index = index_of(name, conditionGroups)

        if(index == -1):
            if reportError == True:
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Value not found in condition groups list: " + name)
            return -1

        return self.GameFile.ConditionGroups[index]

    def get_condition_list(self):
        conditions = []

        for condition in self.GameFile.Conditions:
            conditions.append(condition.Name)

        return conditions

    # For whatever reason, it feels like conditions should not support being held in collections, but this may be changed in future
    def get_condition_ref(self, name, reportError = True):
        if self.invalid_check(name) == False:
            return -1

        conditions = self.get_condition_list()

        index = index_of(name, conditions)

        result = -1

        if(index != -1): # Since the condition evaluation function can receive both types, it doesn't make sense to differentiate them here
            result = self.GameFile.Conditions[index]
        else:
            group = self.get_condition_group_ref(name)

            if(group != -1):
                result = group
            else:
                if reportError == True:
                    rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Value not found in condition list: " + name)
                return -1
            pass
        pass

        return result


####################################################################################################

def initialise():
    global mainRPMasterState

    mainRPMasterState = rpmasterState()

    global randomFloatVariable0_1
    global randomFloatVariable0_100
    global randomIntVariable0_100

    randomFloatVariable0_1 = rpmaster_game_file_variable.Variable("HardcodedFloat0_1")
    randomFloatVariable0_1.PrettyName = "Hardcoded Float 0-1"
    randomFloatVariable0_1.Type = rpmaster_game_file_variable.VariableTypes.HARDCODED

    randomFloatVariable0_100 = rpmaster_game_file_variable.Variable("HardcodedFloat0_100")
    randomFloatVariable0_100.PrettyName = "Hardcoded Float 0-100"
    randomFloatVariable0_100.Type = rpmaster_game_file_variable.VariableTypes.HARDCODED

    randomIntVariable0_100 = rpmaster_game_file_variable.Variable("HardcodedIntVariable0_100")
    randomIntVariable0_100.PrettyName = "Hardcoded Int 0-100"
    randomIntVariable0_100.Type = rpmaster_game_file_variable.VariableTypes.HARDCODED

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Initialised empty state")
    return


def new_state(stateName):
    global mainRPMasterState

    mainRPMasterState = rpmasterState(stateName)
    mainRPMasterState.LiveVariables = copy.deepcopy(
        mainRPMasterState.GameFile.InitialVariables)

    if(mainRPMasterState.GameFile.EnableTime == True):
        CurrentGameDateTime = rpmaster_game_file_variable.Variable("CurrentGameDateTime")
        CurrentGameDateTime.PrettyName = "Current game date-time"
        CurrentGameDateTime.Type = rpmaster_game_file_variable.VariableTypes.DATETIME
        CurrentGameDateTime.Value = mainRPMasterState.GameFile.StartTime
        mainRPMasterState.LiveVariables.append(CurrentGameDateTime)

        EventQueueVariable = rpmaster_game_file_variable.Variable("EventQueueVariable")
        EventQueueVariable.PrettyName = "Event queue variable"
        EventQueueVariable.Type = rpmaster_game_file_variable.VariableTypes.EVENT_QUEUE
        EventQueueVariable.Value = rpmaster_game_file_event.EventQueue()
        mainRPMasterState.LiveVariables.append(EventQueueVariable)
        
    mainRPMasterState.LoadedCorrectly = True
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Created new state: " + stateName)
    return


def save_state():
    global mainRPMasterState

    saveFileDirectory = rpmaster_settings.loaded_settings().get_save_directory()
    savePath = saveFileDirectory + mainRPMasterState.Name + ".yaml"

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Attempting to save state: " + mainRPMasterState.Name)
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Checking for existing save file: " + savePath)

    if(mainRPMasterState.LoadedCorrectly == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Failed to save state - no save file loaded")
        return

    if(path.exists(savePath) == False):
        mainRPMasterState.write_save(savePath)
        rpmaster_log.game_log("Game saved successfully")

    else:
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Found existing save file")
        backup_old_save(savePath)
        if(path.exists(savePath) == False):
            rpmaster_log.debug_log(rpmaster_log.Level.INFO, "No existing save file")
            mainRPMasterState.write_save(savePath)
            rpmaster_log.game_log("Game saved successfully")
        else:
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Save file conflict: " + savePath)

    return


def backup_old_save(savePath):
    global mainRPMasterState

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Backing up old save file")
    if(path.exists(savePath) == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "No existing save file")
        return
    else:
        newPath = savePath + "." + str(mainRPMasterState.StateSaveNumber)
        os.rename(savePath, newPath)
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Backed up old save file: " + newPath)
    return


def load_state(stateName):
    global mainRPMasterState

    saveFileDirectory = rpmaster_settings.loaded_settings().get_save_directory()
    savePath = saveFileDirectory + stateName + ".yaml"

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Loading state: " + stateName)
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Checking for save file: " + savePath)

    if(path.exists(savePath) == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "No save file found: " + savePath)
        return

    else:
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Save file found")
        with open(savePath, 'r') as saveFile:
            # mainRPMasterState = yaml.safe_load(saveFile)
            mainRPMasterState = yaml.load(saveFile, Loader=Loader)
    
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Save loaded successfully")

    mainRPMasterState.LoadedCorrectly = True

    if(mainRPMasterState.SaveFileVersion != currentSaveFileVersion):
        rpmaster_log.debug_log(rpmaster_log.Level.WARNING, "Loaded save file was written by an old version and may cause errors")

    return


def is_save_path_valid(stateName):
    saveFileDirectory = rpmaster_settings.loaded_settings().get_save_directory()
    savePath = saveFileDirectory + stateName + ".yaml"
    if(path.exists(savePath) == False):
        return False
    else:
        return True


def is_state_loaded():
    return mainRPMasterState.LoadedCorrectly


def main_state():
    global mainRPMasterState
    return mainRPMasterState

