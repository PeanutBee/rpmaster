
from PyInquirer import style_from_dict, Token, prompt
from PyInquirer import Validator, ValidationError
from PyInquirer import style_from_dict, Token

rpm_custom_style = style_from_dict({
    Token.Separator: '#6C6C6C',
    Token.QuestionMark: '#FF9D00 bold',
    # Token.Selected: '#0dbc79',
    Token.Selected: '#7acc54',
    Token.Pointer: '#FF9D00 bold',
    Token.Instruction: '',  # default
    Token.Answer: '#5F819D bold',
    Token.Question: '',
})

def index_of(val, in_list):
    try:
        return in_list.index(val)
    except ValueError:
        return -1 
