import yaml
import io
import os
from os import path
from enum import Enum

from clint.textui import colored

import rpmaster_settings
import rpmaster_log

defaultName = 'invalid'
defaultColor = rpmaster_settings.Colors.WHITE

# only standard YAML tag

class RootMenuStatusThreshold(yaml.YAMLObject):
    yaml_tag = u'!rpmasterGameFileRootMenuStatusThreshold'
    Name = ''
    LowerBound = None
    UpperBound = None
    Message = ''
    Color = None

    def __init__(self, Name, LowerBound, UpperBound, Message, Color):
        self.Name = Name
        self.LowerBound = LowerBound
        self.UpperBound = UpperBound
        self.Message = Message
        self.Color = Color

    def __repr__(self):
        return "%s(Name=%r, Message=%r, LowerBound=%r, UpperBound=%r, Message=%r, Color=%r, )" % (
            self.__class__.__name__, self.Name, self.Message, self.LowerBound, self.UpperBound, self.Message, self.Color, )

    def is_valid_threshold(self):
        result = True

        if len(self.Name) == 0:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Root menu status threshold has no name")
            return result

        if self.LowerBound == self.UpperBound:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Lower and upper threshold bounds are the same: " + self.Name)
            return result

        if (self.LowerBound != None) and (self.UpperBound != None):
            if self.LowerBound > self.UpperBound:
                result = False
                rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Threshold lower bound is higher than upper bound: " + self.Name)
                return result

        colorTest = rpmaster_settings.get_color(self.Color)  

        if colorTest == None:
            result = False
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Root menu status threshold has no color: " + self.Name)
            return result
            
        return result
