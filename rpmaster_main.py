from __future__ import print_function, unicode_literals
import regex
from pprint import pprint

from PyInquirer import style_from_dict, Token, prompt
from PyInquirer import Validator, ValidationError
from PyInquirer import style_from_dict, Token

import rpmaster_log
import rpmaster_settings
import rpmaster_menu_root
import rpmaster_game_file
import rpmaster_save
import rpmaster_game_file_operation
from rpmaster_utilities import rpm_custom_style


class SaveExistsValidator(Validator):
    def validate(self, document):
        if(document.text == ''):
            ok = True
        else:
            ok = rpmaster_save.is_save_path_valid(document.text)
        if not ok:
            raise ValidationError(
                message='Save file not found in directory: ' +
                rpmaster_settings.loaded_settings().get_save_directory(),
                cursor_position=len(document.text))  # Move cursor to end


loadQuestions = [
    {
        'type': 'input',
        'name': 'stateName',
        'message': 'Name of save to load (leave blank for fresh start)',
        'validate': SaveExistsValidator
    },
]


class GameFileValidator(Validator):
    def validate(self, document):
        if(document.text == ''):
            ok = True
        else:
            ok = rpmaster_game_file.is_game_file_path_valid(document.text)
        if not ok:
            raise ValidationError(
                message='Game file not found in directory: ' +
                rpmaster_settings.loaded_settings().get_game_file_directory(),
                cursor_position=len(document.text))  # Move cursor to end


class SaveDoesNotExistValidator(Validator):
    def validate(self, document):
        if(document.text == ''):
            ok = True
        else:
            ok = (rpmaster_save.is_save_path_valid(document.text) == False)
        if not ok:
            raise ValidationError(
                message='Save already exists in directory: ' +
                rpmaster_settings.loaded_settings().get_save_directory(),
                cursor_position=len(document.text))  # Move cursor to end


newStateQuestions = [
    {
        'type': 'input',
        'name': 'gameFileName',
        'message': 'Enter the name of the game file you would like to play (leave both this and next answer blank to generate a new one and exit)',
        'validate': GameFileValidator
    },
    {
        'type': 'input',
        'name': 'stateName',
        'message': 'Enter a name for the new game',
        'validate': SaveDoesNotExistValidator
    },

]


def main():

    print("RPM initialising...")

    rpmaster_settings.initialise()
    rpmaster_log.initaliseDebugLog()
    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Starting RPM")

    rpmaster_game_file.initialise()
    rpmaster_save.initialise()

    print("Ready!")
    print("")

    answers = prompt(loadQuestions, style=rpm_custom_style)

    stateName = answers.get('stateName', '')

    if(stateName == ''):
        print("Starting a fresh game") # game logging depends on having a save load, so ony print for init

        answers = prompt(newStateQuestions, style=rpm_custom_style)
        gameFileName = answers.get('gameFileName', '')
        stateName = answers.get('stateName', '')

        if(gameFileName == ''):
            print("No game file name entered, generating example file: " +
                  rpmaster_game_file.defaultGameFileName)
            gameFileName = rpmaster_game_file.defaultGameFileName
            rpmaster_game_file.generate_game_file()
            print("Example file is ready, please reload")
            return

        if(stateName == ''):
            print("No save name entered, using: " + rpmaster_save.defaultStateName)
            stateName = rpmaster_save.defaultStateName

        rpmaster_game_file.load_game_file(gameFileName)

        if(rpmaster_game_file.is_game_file_loaded() == False):
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Game file load error")
            return

        rpmaster_save.new_state(stateName)
        rpmaster_save.save_state()

    else:
        rpmaster_save.load_state(stateName)

    if(rpmaster_save.is_state_loaded() == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Save load error")
        return

    rpmaster_log.initaliseGameLog()

    if(rpmaster_save.main_state().is_valid_save() == False):
        rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Save failed validation check: " + rpmaster_save.main_state().Name)
        return

    if rpmaster_save.main_state().InitialisationComplete == False:
        initOperationName = rpmaster_save.main_state().GameFile.InitialisationOperation
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "First start of game, attempting to run initialisation operation: " + initOperationName)

        opRef = rpmaster_save.main_state().get_operation_ref(initOperationName) 
        
        if opRef != -1:
            rpmaster_game_file_operation.execute_operation(opRef)
            rpmaster_save.main_state().InitialisationComplete = True
            pass
        else:
            rpmaster_log.debug_log(rpmaster_log.Level.ERROR, "Unable to fine initialisation operation: " + initOperationName)
            pass
        pass

        rpmaster_save.main_state().InitialisationComplete = True

    rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Initialisation complete, loading menu")

    while(1):
        rpmaster_menu_root.menu_root()

    return


if __name__ == "__main__":
    main()

rpmaster_log.debug_log(rpmaster_log.Level.INFO, "Exiting RPM")
