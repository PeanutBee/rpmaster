from __future__ import print_function, unicode_literals
import regex
import sys
import os
import psutil
import logging
from pprint import pprint

from PyInquirer import style_from_dict, Token, prompt, print_json, Separator, color_print
from PyInquirer import Validator, ValidationError

from rpmaster_utilities import rpm_custom_style

from clint.eng import join
from clint.textui import colored, indent, puts
from clint.textui import columns

import rpmaster_settings
import rpmaster_save
import rpmaster_log
import rpmaster_game_file_operation

from rpmaster_utilities import index_of

optionIdNames = []
options = []

def get_menu_choices(answers):
    global optionIdNames
    global options

    optionIdNames.clear()
    options.clear()

    operationList = rpmaster_save.main_state().get_operation_list()

    for operation in operationList:
        opRef = rpmaster_save.main_state().get_operation_ref(operation, printLog = False)

        if(opRef.is_visible() == True):
            options.append(opRef.PrettyName)
            optionIdNames.append(opRef.Name)
        pass

    options.append(Separator())
    options.append('Return')
    return options


def menu_operations():
    global optionIdNames
    global options

    menu_operations_choices = [
        {
            'type': 'list',
            'name': 'menu_option',
            'message': 'What would you like to do?',
            'choices': get_menu_choices,
        },
    ]

    answers = prompt(menu_operations_choices, style=rpm_custom_style)
    command = answers.get('menu_option', '')

    if(command == 'Return'):
        return

    index = index_of(command, options)

    menu_do_operation(optionIdNames[index])

    return


def menu_do_operation(operationName):
    if(rpmaster_settings.loaded_settings().AutoSaveOnOperation == True):
        rpmaster_log.debug_log(rpmaster_log.Level.INFO, "On operation auto save")
        rpmaster_save.save_state()

    rpmaster_save.main_state().store_variables_to_history(operationName)

    rpmaster_game_file_operation.execute_operation(
        rpmaster_save.main_state().get_operation_ref(operationName))

    green = getattr(colored, 'green')
    input(green("Operation complete...")) # Causes the game log to hold until user progresses
    return
